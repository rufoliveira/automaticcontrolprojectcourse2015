
% Initializes the connection to the Qualisys Computer
% Make sure the Qualisys Computer is on, and that the Capture program 
% is running.
% For more details, read QMC_conf.txt
qtm_handler=QMC('QMC_conf.txt');

body_id = 18;

for i = 1:100
   
    % Gets the pose estimate of a body, given its body id
    [position, orientation, t_stamp] = get_qualisys_body(qtm_handler, body_id);
    
    disp('position')
    disp(position)
    
    pause(0.1)
    
end