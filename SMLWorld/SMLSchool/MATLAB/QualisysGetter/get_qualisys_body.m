function [position, orientation, t_stamp] = get_qualisys_body(qtm, body_id)
%GET_POSE_QUALISYS Gets the position, orientation and time stamp of a
%body measurement from Qualisys

    the6DOF=QMC(qtm);
    frameinfo=QMC(qtm,'frameinfo');
    t_stamp=frameinfo(2)/(10^6);
    
    x=the6DOF(1,body_id)/1000;
    y=the6DOF(2,body_id)/1000;
    z=the6DOF(3,body_id)/1000;
    
    position = [x; y; z];
    
    roll=deg2rad(the6DOF(4,body_id));
    pitch=deg2rad(the6DOF(5,body_id));    
    yaw=deg2rad(the6DOF(6,body_id));  
    
    orientation = [roll, pitch, yaw];    
    
end

