import signal
import sys
from PyQt4 import QtGui, uic
from PyQt4.QtCore import *
import inspect
from threading import Thread, Timer
import time
from PyQt4.QtGui import QApplication, QMessageBox
import pprint
import math
import smartvehicle
import RoadSideUnit
from Constants import *
import Configuration 

import util

class HMI(Thread):
    def __init__(self,SML):
        Thread.__init__(self)
        self.SML = SML
        self.hmi = []

    def run(self):
        self.app = QtGui.QApplication(sys.argv)
        self.hmi.append(HMIWindow(self.SML))
        return self.app.exec_()

    def update(self):
        while self.hmi==[]:
            time.sleep(0.1)
        if self.hmi!=[]:
            self.hmi[0].setVehicleList()

    def kill(self):
        self.hmi[0].close()


class HMIWindow(QtGui.QMainWindow):
    def __init__(self,SML):
        super(HMIWindow, self).__init__()

        self.SML = SML

        uic.loadUi('hmi.ui', self)
        self.brake.clicked.connect(self.brakeVehicle)
        self.pGas.clicked.connect(self.gasVehicle)
        self.merge.clicked.connect(self.mergeOK)
        self.bStart.clicked.connect(self.start_sim)
        self.bRSUStart.clicked.connect(self.start_rsu)
        self.emergency.clicked.connect(self.spawn_emergency_vehicle)


        self.pSensor.clicked.connect(self.cSensor)
        self.pSys.clicked.connect(self.cSys)
        self.pActuator.clicked.connect(self.cActuator)
        self.pCommunication.clicked.connect(self.cCommunication)

        self.pEOS.clicked.connect(self.cEOS)

        self.pEmergency.clicked.connect(self.emergencyButton)

        self.timer = QTimer()
        self.timer.start(100) # lancer un top toutes les secondes (=1000 millisecondes)
        self.connect(self.timer, SIGNAL("timeout()"), self.getState)
        self.connect(self.timer, SIGNAL("timeout()"), self.appendConsole)

        self.setScenarioGUI()

        self.velocity_set=0

        self.show()

    def closeEvent(self, event):
        self.SML.quit()
        super(QtGui.QMainWindow, self).closeEvent(event)

    def setVehicleList(self):
        keyList = sorted(self.SML.bodies_dict.keys())
        self.listVehicle.clear()
        for k in keyList:
            self.listVehicle.addItem(str(k))

    def setScenarioGUI(self):
        if self.SML.scenario==2:
            self.emergency.setEnabled(False)

    def emergencyButton(self):
        idx = self.getVehicleSelected()
        if idx!=None:
            self.SML.bodies_dict[idx].supervisory_module.manual_button_pressed = not self.SML.bodies_dict[idx].supervisory_module.manual_button_pressed 
            print "Emergency : "+str(idx)+ " = " + str(self.SML.bodies_dict[idx].supervisory_module.manual_button_pressed)

    def getVehicleSelected(self):
        if len(self.listVehicle.selectedItems())!=0:
            return int(self.listVehicle.selectedItems()[0].text())
        else:
            return None

    def brakeVehicle(self):
        idx = self.getVehicleSelected()
        if idx!=None:
            self.SML.bodies_dict[idx].control_module.brake = not self.SML.bodies_dict[idx].control_module.brake
            self.SML.bodies_dict[idx].supervisory_module.brake_pressed = self.SML.bodies_dict[idx].control_module.brake

            print "Brake : "+str(idx)+ " = " + str(self.SML.bodies_dict[idx].control_module.brake)

    def gasVehicle(self):
        idx = self.getVehicleSelected()
        velocity = self.velocity.value()
        if idx!=None :
            self.SML.bodies_dict[idx].supervisory_module.gas_pressed = not self.SML.bodies_dict[idx].supervisory_module.gas_pressed
            if self.SML.bodies_dict[idx].supervisory_module.gas_pressed:

                if self.SML.bodies_dict[idx].control_module.CC is not True:
                    if self.SML.bodies_dict[idx].control_module.ACCC is True:
                        self.old_controller = 'ACCC'
                        self.ACCC_target_id = self.SML.bodies_dict[idx].control_module.ACCC_target_id
                        self.ACCC_desired_distance = self.SML.bodies_dict[idx].control_module.ACCC_desired_distance

                    elif self.SML.bodies_dict[idx].control_module.OA is True:
                        self.old_controller = 'OA'
                        self.OA_target_id = self.SML.bodies_dict[idx].control_module.OA_target_id
                        self.OA_desired_distance = self.SML.bodies_dict[idx].control_module.OA_desired_distance
                    elif self.SML.bodies_dict[idx].control_module.VACCC is True:
                        self.old_controller = 'VACCC'
                        self.VCACC_target_id = self.SML.bodies_dict[idx].control_module.VACCC_target_id
                        self.VCACC_desired_distance = self.SML.bodies_dict[idx].control_module.VACCC_desired_distance

                    self.SML.bodies_dict[idx].control_module.activate_CC_controller(velocity)
                else:
                    self.old_controller = 'CC'
                    self.old_velocity = self.SML.bodies_dict[idx].control_module.CC_desired_speed
                    self.SML.bodies_dict[idx].control_module.set_CC_desired_speed(velocity)

            else:
                if self.old_controller is 'ACCC':
                    self.SML.bodies_dict[idx].control_module.activate_CACC_controller(self.ACCC_desired_distance, self.ACCC_target_id)
                elif self.old_controller is 'OA':
                    self.SML.bodies_dict[idx].control_module.activate_OA_controller(self.OA_desired_distance, self.OA_target_id)
                elif self.old_controller is 'VACCC':
                    self.SML.bodies_dict[idx].control_module.activate_VCACC_controller(self.VCACC_desired_distance, self.VCACC_target_id)
                elif self.old_controller is 'CC':
                    self.SML.bodies_dict[idx].control_module.set_CC_desired_speed(self.old_velocity)



            print "Gas : "+str(idx)+ " = " + str(self.SML.bodies_dict[idx].supervisory_module.gas_pressed)


    def change_velocity(self):
        idx = self.getVehicleSelected()
        if idx!=None:
            #self.SML.bodies_dict[idx].control_module.set_CC_desired_speed(float(self.velocity.value()))

            print "Velocity : "+str(idx)+ " = " + str(self.velocity.value())

    def getState(self):
        idx = self.getVehicleSelected()
        if idx in self.SML.bodies_dict.keys():
            vehicle = self.SML.bodies_dict[idx]
            if isinstance(vehicle,RoadSideUnit.RoadSideUnit)==False:
                linear_speed = math.hypot(vehicle.x_speed, vehicle.y_speed)
                state = {"brake":vehicle.control_module.brake,
                         "speed": "%.2lf"%(linear_speed),
                         "thottle": "%.2lf"%(vehicle.commands['throttle'])}
                

                scenario = {}
                if self.SML.scenario==1:
                    scenario = {"State":vehicle.supervisory_module.GCDC_module.scenario_module.current_state,
                                "Abort":vehicle.supervisory_module.GCDC_module.scenario_module.abort.state,
                                "fwd_pair":vehicle.fwd_pair_partner,
                                "bwd_pair":vehicle.bwd_pair_partner,
                                "mio":vehicle.supervisory_module.MIO_id,
                                "tail_position":vehicle.tail_vehicle,
                                "safety_distance":"%.2lf"%(vehicle.supervisory_module.GCDC_module.scenario_module.get_distance_to_gap_making()),
                                "leader":vehicle.supervisory_module.GCDC_module.scenario_module.we_are_leader}
                elif self.SML.scenario==2:
                    scenario = {"State":vehicle.supervisory_module.GCDC_module.scenario_module.current_state,
                                "travelled_distance_CZ": vehicle.supervisory_module.travelled_CZ} 

                controller = {"ACCC":str(vehicle.control_module.ACCC)+" target "+str(vehicle.control_module.ACCC_target_id),
                              "VACCC":str(vehicle.control_module.VACCC)+" target "+str(vehicle.control_module.VACCC_target_id),
                              "CC":str(vehicle.control_module.CC)+" target "+str(vehicle.control_module.CC_desired_speed),
                              "MIX":str(vehicle.control_module.mix_controller),
                              "OA"  :str(vehicle.control_module.OA)  +" target "+str(vehicle.control_module.OA_target_id)}
                
                scrollbarposition = self.state.verticalScrollBar().value()
                self.state.setText(pprint.pformat([state,scenario,controller],width=1))
                self.state.verticalScrollBar().setValue(scrollbarposition)
                # button/velocity show
            else:
                self.state.setText("Road Side Unit")

        else:
            self.state.setText("")

    def mergeOK(self):
        idx = self.getVehicleSelected()
        if idx in self.SML.bodies_dict.keys():
            vehicle = self.SML.bodies_dict[idx]
            if self.SML.scenario == 1:
                vehicle.supervisory_module.GCDC_module.scenario_module.mergeOK = not vehicle.supervisory_module.GCDC_module.scenario_module.mergeOK

    def start_sim(self):
        self.SML.start_simulation()

    def start_rsu(self):
        for vehicle in self.SML.bodies_dict.values():
            vehicle.supervisory_module.start_scenario = True

    def spawn_emergency_vehicle(self):
        conf = Configuration.read_configuration_file()
        velocity = conf['emergency_speed']
        distance = conf['emergency_distance']
        lane = conf['emergency_lane']

        traj_idx = []
        for vehicle in self.SML.bodies_dict.values():
            if vehicle.id!=0:
                traj_idx.append(vehicle.control_module.get_closest_point_on_trajectory())

        emergency_vehicle = smartvehicle.SmartVehicle(self.SML, 0,
                lane_start_point_index = 0,init_velocity=velocity/3.6, emergency_vehicle=True, init_lane=lane)

        emergency_vehicle.setDesiredVelocity(velocity/3.6)
        idx = min(traj_idx)-distance*5
        emergency_vehicle.set_vehicle_on_trajectory_state(idx)
        self.SML.bodies_dict[0] = emergency_vehicle

        self.setVehicleList()

    def appendConsole(self):
        d = util.get_print()
        for l in d:
            self.console.append(l)

        if len(d)!=0:
            self.console.moveCursor(QtGui.QTextCursor.End)

    def cEOS(self):
        for vehicle in self.SML.bodies_dict.values():
            if isinstance(vehicle,RoadSideUnit.RoadSideUnit)==False:
                vehicle.supervisory_module.EoS = True
                print 'EoS true'
        print "EoS"

    def cSensor(self):
        idx = self.getVehicleSelected()
        if idx in self.SML.bodies_dict.keys():
            vehicle = self.SML.bodies_dict[idx]
            vehicle.supervisory_module.perception_error_sim = not vehicle.supervisory_module.perception_error_sim
            print "Perception errors : "+ str(vehicle.supervisory_module.perception_error_sim)

    def cSys(self):
        idx = self.getVehicleSelected()
        if idx in self.SML.bodies_dict.keys():
            vehicle = self.SML.bodies_dict[idx]
            vehicle.supervisory_module.system_error_sim = not vehicle.supervisory_module.system_error_sim
            print "System errors : "+ str(vehicle.supervisory_module.system_error_sim)

    def cActuator(self):
        idx = self.getVehicleSelected()
        if idx in self.SML.bodies_dict.keys():
            vehicle = self.SML.bodies_dict[idx]
            vehicle.supervisory_module.control_error_sim = not vehicle.supervisory_module.control_error_sim
            print "Control errors : "+ str(vehicle.supervisory_module.control_error_sim)

    def cCommunication(self):
        idx = self.getVehicleSelected()
        if idx in self.SML.bodies_dict.keys():
            vehicle = self.SML.bodies_dict[idx]
            vehicle.supervisory_module.communication_error_sim = not vehicle.supervisory_module.communication_error_sim
            print "Communication errors : "+ str(vehicle.supervisory_module.communication_error_sim)

if __name__ == '__main__':
    hmi = HMI([])
    hmi.start()

    signal.signal(signal.SIGINT, lambda s,f: hmi.kill())
    signal.pause()
