
class StateScen1(object):
	INIT								= ("INIT",1)
	WAIT_RSU							= ("WAIT_RSU",2)

	A_WAIT_OPC							= ("A_WAIT_OPC",3)
	A_WAIT_PAIR							= ("A_WAIT_PAIR",4)
	A_WAIT_MERGE						= ("A_WAIT_MERGE",5)
	A_WAIT_STOM							= ("A_WAIT_STOM",6)
	A_MERGING							= ("A_MERGING",7)
	A_TAIL_GAP_MAKING					= ("A_TAIL_MERGING",8)

	B_WAIT_OPC	 						= ("B_WAIT_OPC",9)
	B_WAIT_FWD_PAIR 					= ("B_WAIT_FWD_PAIR",10)
	B_WAIT_ACK_PAIRING 					= ("B_WAIT_ACK_PAIRING",11)
	B_GAP_MAKING						= ("B_GAP_MAKING",12)
	B_A_IS_MERGING						= ("B_A_IS_MERGING",13)
	B_A_HAS_MERGED						= ("B_A_HAS_MERGED",14)
	WAIT_RESTART_SCENARIO				= ("WAIT_RESTART_SCENARIO",15)


	EOS					= "EOS"

class Lane(object):
	RIGHT  	= "right_lane"
	CENTER 	= "center_lane"
	LEFT 	= "left_lane"
	A = "right_lane"
	B = "center_lane"
