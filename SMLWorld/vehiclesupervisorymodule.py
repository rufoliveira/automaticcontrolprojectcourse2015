import time
import smartvehicle
import vehiclecontrolmodule
import MessageModule
import threading

import sys
import math
from Constants import *
import copy 

sys.path.append('Supervisory')
import GCDC_supervisory_layer

# This is the new supervisory module, that will work as an interface to our own code.
# TIPS from the old supervisory layer:

# Starting velocity and lane is set in smartvehicle (currently lines 120-125)
# To set velocity use the self.setVelocity(new_velocity) function
# self.getVelocty() returns your current velocity, self.vehicle.desired_velocity points to the velocity the control module is trying to keep
# To change lane use the self.addLaneChange(new_lane) function
# To change/set a new pair partner, use the self.setPairPartner(target_id, fwd = True/False, bwd = True/False, new = True/False) function

# Things from perception_module:
# 	- self.vehicle.perception_grid is a dictionary with keys 'fl','fm','fr','l','r','rl','rm','rr', with values lists of vehicle ids in that part of the grid
#	- self.vehicle.perceived_objects is a dictionary with vehicle ids as keys, and lists of relative x_coord, relative y_coord and velocity as values
#	- self.vehicle.closest_objects is a dictionary with the same keys as perception_grid, but only contains the id of the vehicle closest to you in that part of the grid
#	- self.vehicle.sensor_readings is a list with each element as a list of readings. Those readings contain vehicle id, x, y, speeds etc

# right lane is 2026 points, center lane is 1899 points, so to keep the vehicles approximately at the same distance from each other (over all) a center lane vehicle should have 0.937 of the right lane vehicle velocity



class VehicleSupervisoryModule:
	"This class is the Platooning Vehicle class used by the Platooning Manager Module"
	def __init__(self, vehicle):
		## gains access to the vehicle variables
		self.vehicle = vehicle
		#time for the initiating of supervisory
		self.module_start_time = time.time()


########### Initialize attributes ####################

		# Most important object in the front, currently necessary for messageModule
		self.MIO_id = {}
		self.MIO_range = None
		self.MIO_bearing = None
		self.MIO_range_rate = None

		# states of the vehicle
		self.velocity=None
		self.position=None
		self.acceleration=None
		self.orientation=None

		#initialize all attributes GCDC needs
		self.target_distance=None
		self.ACCC_target_id=None
		self.time_headway = None	# time to vehicle in front, messages
		self.cruise_speed = None	# nominal speed of platoon, messages
		self.travelled_CZ = None	# Distance travelled in Competition zone
		self.type_controller =None # keeping track of which controller we use
		self.platoon_id = None

		# errors
		self.perception_error=False
		self.control_error=False
		self.communication_error=False
		self.system_error_sim=False
		self.perception_error_sim=False
		self.control_error_sim=False
		self.communication_error_sim=False


		# needed for scenario 2
		self.current_lane = None
		self.intention	= self.vehicle.getIntention() # where are we heading S2.
		self.distance_to_intersection=None
		self.intersection_position=[0,0] #should be the coordinates of the intersection
		self.lane_entering_CZ = self.vehicle.getLaneEnteredCZ()
		self.intersection_vehicle_counter = None

		#HMI variables
		self.gas_pressed=False
		self.brake_pressed=False
		self.manual_button_pressed=False

		#message attributes
		self.STOM=False
		self.intersection_vehicle_counter=None
		self.dummy_time = time.time()
		self.reference_time = time.time()			#id 37
		self.merge_flag_request	= False
		self.merging_flag = False
		self.reference_time	= None
		self.event_type = None

		self.emergency_brake_flag  = False
		self.stationary_vehicle_flag = False

		self.start_scenario = False
		self.EoS = False
############### starting the GCDC supervisory layer #####################

		#initialize GCDC
		self.GCDC_module=GCDC_supervisory_layer.GCDC_supervisory_layer(self,self.vehicle.sml_world.scenario)

		# scenario 3 variable needed
		self.last_emergency_message_timestamp = -1
############# Staring the messaging threads ################


		#set number of cars in the scenario
		if self.GCDC_module.scenario==2:
			self.number_of_cars=3
		else:
			self.number_of_cars=self.vehicle.sml_world.conf['nlanea']+self.vehicle.sml_world.conf['nlaneb']

		#initialize messagemodule
		self.message_module=MessageModule.MessageModule(self.vehicle, self)
		self.message_incoming=None
		self.wifi_incoming=None

		#initialize threads for communication
		self.vehicle.start_thread(self.message_frequency,args=([10]))
		self.vehicle.start_thread(self.message_frequency,args=([1]))
		self.vehicle.start_thread(self.message_handler, args=([]))





		print "VehicleSupervisoryModule started"


	def step(self): 	## This is the step function that is running at each cycle of the thread in SmartVehicle
		self.update_values() # update values
		
		#call GCDC step
		if time.time()-self.dummy_time>2.0:
			self.GCDC_module.step()


	def update_values(self):
		##help-function that gather all values needed by the supervisory layer. Writen by Carolina Eriksson 11-2015
		
		#state of vehicle
		self.velocity=self.get_velocity()
		self.position=[self.vehicle.x, self.vehicle.y]
		self.acceleration=None
		self.orientation=self.vehicle.yaw
		self.current_lane=self.vehicle.getCurrentLaneTrajectoryString()#current lane
		self.intention=self.vehicle.getIntention()
		self.lane_entering_CZ=self.vehicle.getLaneEnteredCZ()

		####----values from the perception layer----
		if hasattr(self.vehicle,'perception_module'):
			## not working at the moment
			self.target_distance=self.vehicle.perception_module.getLongitudinalDistanceToTarget(self.ACCC_target_id) #distance to target
			#self.distance_to_intersection=self.vehicle.perception_module.distance(self.position, self.intersection_position)#distance to intersection center if scenario 2 is run -needed?

			## needed for scenario 1
			if self.MIO_id!=None:
				if not ('fm' in self.MIO_id):
					self.vehicle.platoon_leader = True
				else:
					self.vehicle.platoon_leader = False

		#####---values from control layer----
		if hasattr(self.vehicle, 'control_module'):
			self.ACCC_target_id=self.vehicle.control_module.ACCC_target_id	#target ID
			self.brake_pressed=self.vehicle.control_module.brake
			# I am at the tail position
			self.vehicle.tail_vehicle = not self.am_i_changing_lane() and not ('rm' in self.MIO_id)


		####----- Error Messages----
		self.perception_error=self.vehicle.getError()[0]#error message state of perception (true/false)
		self.control_error=self.vehicle.getError()[1]#error message state of control (true/false)
		#self.communication_error is updated in communication below if changed. (true/false)

		####----HMI----
		#self.gas_pressed=self.vehicle.getHMI(gas)
		#self.brake_pressed=self.vehicle.getHMI(brake)
		#self.manual_button_pressed=self.vehicle.getHMI(manual)

		############## Scenario 1 ##############
		self.platoon_id = "B" if self.get_lane()==Lane.A else "A" 		#Set the platoon Ids


		###### flags ############
		self.update_emergency_flag()
		self.stationary_vehicle_flag = self.i_am_stationary()

		if self.stationary_vehicle_flag:
			self.event_type = 95
		elif self.emergency_brake_flag:
			self.event_type = 99
		else:
			self.event_type = 0

	


###########################################################################functions for GCDC to call for setting/sending messages and giving control commands

	def set_leader_flag(self, bool_value): #message
		self.vehicle.platoon_leader=bool_value

	def set_target_id(self, ID):#message (MIO)
		self.vehicle.control_module.ACCC_target_id=ID

	def set_fwd_pair_partner(self, ID): #message
		self.vehicle.fwd_pair_partner=ID

	def set_bwd_pair_partner(self, ID): #message
		self.vehicle.bwd_pair_partner=ID

	#def set_merge_request_flag(): #message (unclear what it should be)

	#def set_merging_flag(): #message (unclear what it should be)

	def set_STOM(self, bool_value): #message (indicates that distance ahead is large enough for vehicle to enter)
		self.STOM=bool_value

	def set_tail_vehicle_flag(self, bool_value): #message
		self.vehicle.tail_vehicle=bool_value

	def set_intersection_vehicle_counter(self, counter): #message
		self.intersection_vehicle_counter=counter

	#def set_HMI_message(self, message):
		#print message to GUI

	def change_lane(self, new_lane):
		self.set_desired_lane(new_lane)
		self.vehicle.control_module.update_lane()

	def get_MIO_id(self):
		if hasattr(self.vehicle, 'message_module'):
			return self.MIO_id
		else:
			return None

	def get_MIO_FWDM(self):
		if hasattr(self.vehicle, 'message_module'):
			if 'fm' in self.MIO_id:
				return self.MIO_id['fm']
		else:
			return None

	####################################################################################functions to set some attributes (can be used if only one is to be chnaged instead of updating all as in functions below) Written by Sofie/Carolina

	def set_desired_velocity(self, desired_velocity):
		self.vehicle.desired_velocity = desired_velocity

	def set_desired_distance(self, desired_distance):
		self.vehicle.control_module.ACCC_desired_distance = desired_distance

	def set_desired_virtual_distance(self, desired_virtual_distance):
		self.vehicle.control_module.VACCC_desired_distance=desired_virtual_distance

	def set_virtual_distance(self, virtual_distance):
		# written by David
		self.vehicle.control_module.VACCC_distance=virtual_distance


	# def set_reachCZ_control(self,reachCZ):
	# 	# written by David
	#	# commented by David DELETE ME LATER
	# 	# used to give the reach CZ control signal to the control module
	# 	self.vehicle.control_module.u_reachCZ=reachCZ

	def activate_reachCZ_control(self):
		self.vehicle.control_module.use_reachCZ=True

	def deactivate_reachCZ_control(self):
		self.vehicle.control_module.use_reachCZ=False


	def set_desired_OA_distance(self, distance):
		self.vehicle.control_module.OA_desired_distance = distance

	def set_desired_OA_target_ID(self, ID):
		self.vehicle.control_module.OA_target_id = ID

	def set_current_lane(self,lane):
		# Carolina,
		self.current_lane=lane

	def set_intention(self,intention):
		# Carolina, used for setting the settings for S2
		self.intention=intention

	def set_lane_entering_CZ(self,lane):
		# Carolina, used for setting the settings for S2
		self.lane_entering_CZ=lane

	def set_intersection_vehicle_counter(self,intersection_vehicle_counter):
		# written by David
		self.intersection_vehicle_counter=intersection_vehicle_counter

	def set_distance_to_CZ(self,distance_to_CZ):
		# written by David
		self.distance_to_intersection=distance_to_CZ

	def set_travelled_distance_in_CZ(self,travelled_distance):
		self.travelled_CZ=travelled_distance

	def has_simulation_started(self):
		return self.vehicle.sml_world.simulation_started

	def is_car_ready(self):
		return hasattr(self.vehicle,'perception_module') and \
			   hasattr(self.vehicle,'control_module')


	################################################## Function Paul wrote without commenting them.

	def is_vehicle_emergency_brake(self,id):
		msg = self.GCDC_module.scenario_module.get_message(id)
		if msg == None:
			return None
		if not "event_type" in msg:
			return None
		return msg["event_type"]==99
	
	def is_vehicle_stopping(self, id):
		msg = self.GCDC_module.scenario_module.get_message(id)
		if msg == None:
			return None
		if not "event_type" in msg:
			return None
		return msg["event_type"] in [99,95]

########################################################################################some functions gathering the functions above for easier calls. Written by Sofie

	def set_CACC_control(self, ID, distance):
		self.vehicle.control_module.activate_CACC_controller(distance,ID)
		self.type_controller='CACC'

	def set_VCACC_control(self, ID, distance):
		self.vehicle.control_module.activate_VCACC_controller(distance,ID)
		self.type_controller='VCACC'

	def set_OA_control(self, ID, distance):
		self.vehicle.control_module.activate_OA_controller(distance,ID)
		self.type_controller='OA'

	def update_OA_target_id(self, ids):
		self.vehicle.control_module.update_OA_target_vehicle(ids)
	def reset_OA(self):
		self.vehicle.control_module.desactivate_OA_controller()

	def set_CC_control(self, speed):
		self.vehicle.control_module.activate_CC_controller(speed)
		self.type_controller='CC'

	def set_manual_control(self):
		#self.vehicle.control_module.activate_manual_controller(speed)
		self.vehicle.control_module.activate_CA_controller()
		self.type_controller='manual'

	def set_temp_manual_control(self):
		#self.vehicle.control_module.activate_manual_controller(speed)
		self.type_controller='Temporal manual'

	def set_CA_control(self):
		self.vehicle.control_module.activate_CA_controller()
		self.type_controller='CA'

######################################################################################## getter functions
	def get_lane(self):
		return self.vehicle.desired_lane

	def get_moil_fwd(self):
		if self.do_i_have_a_miol_fwd():
			return self.MIO_id['fl']
		else:
			return False
	def bwd_STOM(self):
		id = self.vehicle.bwd_pair_partner
		return self.vehicle.bodies_dict[id].supervisory_module.STOM

	def get_position(self):
		#written by David
		return self.position

	def get_orientation(self):
		#written by David
		return self.orientation

	def get_intention(self):
		#written by David
		return self.intention

	def get_lane_entering_CZ(self):
		#written by David
		return self.lane_entering_CZ

	def get_messages(self):
		#written by David
		return self.message_incoming

	def get_vehicle_id(self):
		# written by David
		return self.vehicle.id

	def get_EoS_flag(self):
		return self.EoS
######################################################################################## Scenario 1 functions
	# get the vehicle that is forward
	def get_fwd_middle(self):
		if ('fm' in self.MIO_id):
			return self.MIO_id['fm']

## return true if the perception module is available and that the forward left object is visible
	# Paul: this function check if I have a fwd MIO
	def do_i_have_a_miol_fwd(self):
		return ('fl' in self.MIO_id)
	
	# Paul: this function check if the fwd MIO partner is not paired
	def is_miol_fwd_not_paired(self):
		id = self.get_moil_fwd()
		if id!=False:
			if self.vehicle.bodies_dict[id].bwd_pair_partner == False:
				return True
		return False

	# Paul: Do the A2B pairing
	def pairupB(self):
		id = self.get_moil_fwd()
		self.set_fwd_pair_partner(id)
		self.set_OA_control(id,10.0)

	# Paul: Check if the gap is made
	def is_gap_made_A(self):
		if 'fm' in self.MIO_id:
			d_fwd_p = self.vehicle.perception_module.getLongitudinalDistanceToTarget(self.vehicle.fwd_pair_partner)
			d_fwd = self.vehicle.perception_module.getLongitudinalDistanceToTarget(self.MIO_id['fm'])
			if d_fwd_p>1.0 and d_fwd > 10.0:
				return True
		return False

	# Paul: Check if the gap is made
	def is_gap_made_B(self):
		d_fwd_p = self.vehicle.perception_module.getLongitudinalDistanceToTarget(self.vehicle.fwd_pair_partner)
		if d_fwd_p>1.0:
			return True
		return False

	# Paul: Do the B2A pairing
	def pairupA(self):
		bwd_pair_id = self.vehicle.bwd_pair_partner
		if 'fm' in self.vehicle.bodies_dict[bwd_pair_id].closest_objects:
			fwd_pair_id = self.vehicle.bodies_dict[bwd_pair_id].closest_objects['fm']
			self.set_fwd_pair_partner(fwd_pair_id)
			self.set_OA_control(fwd_pair_id,10.0)

	# Paul: set the received pair from B lane
	def receive_pair_from_B(self):
		self.vehicle.bwd_pair_partner = False
		for vehicle in self.vehicle.bodies_dict.values():
			if self.vehicle.id == vehicle.fwd_pair_partner:
				self.vehicle.bwd_pair_partner = vehicle.id

	# Paul: start the merging
	def merge(self):
		self.merging_flag = True
		self.set_desired_lane(Lane.A)

	def force_merge(self):
		self.merge()

	# Paul: reset the pairing information of the car
	def reset_pairing(self):
		self.reset_OA()
		self.set_fwd_pair_partner(None)
		self.set_bwd_pair_partner(None)

	# Paul: reset the messages linked to the Scenario 1
	def reset_messages(self):
		self.set_STOM(False)

	# Paul: check if the fwd pair is merged
	def is_fwd_pair_merged(self):
		return self.vehicle.bodies_dict[self.vehicle.fwd_pair_partner].supervisory_module.am_i_merged()

	# Paul: check if I am merged
	def am_i_merge(self):
		if self.vehicle.control_module.getTraj() == Lane.RIGHT:
			return True
		return False


	def am_i_changing_lane(self):
		return self.vehicle.control_module._changing_lanes

########################################################################################internal functions
	def get_velocity(self):
		''' Returns the current velocity '''
		velocity = math.hypot(self.vehicle.x_speed, self.vehicle.y_speed)
		return velocity

	def set_desired_lane(self, desired_lane): #desired_lane is a string ("right_lane" osv)
		self.vehicle.control_module.set_desired_lane(desired_lane,desired_change_length=30)

############################################### Message functions

	def send_message(self,freq): #freq=1,25 or 10
		'''send message '''
		message_list=self.message_module.generateMessage(freq)
		self.vehicle.v2v_network_output.append(message_list)

	def message_frequency(self,freq):
		counter=1
		while True:
			start_time=time.time()
			if freq==10:
				self.send_message(10)
				time_end=time.time()
				elapsed_time=time_end-start_time
				sleep_time=0.1-elapsed_time
				if sleep_time>0:
					time.sleep(sleep_time)
					self.communication_error=False
				else:
					self.communication_error=True
			else:
				if counter==25:
					self.send_message(1)
					counter=1
				else:
					self.send_message(25)
					counter+=1
				time_end=time.time()
				elapsed_time=time_end-start_time
				sleep_time=0.04-elapsed_time
				if sleep_time>0:
					time.sleep(sleep_time)
					self.communication_error=False
				else:
					self.communication_error=True

	def parseWiFiMessages(self,messages):
		if messages is None:
			return 
		# print 'amount of messages ' + str(len(messages))
		EV_messages=[]
		for m in messages:
			if 'station_type' in m:
				if m['station_type']==10:
					if m['event_type']==95:
						EV_messages.append(m)
		if len(EV_messages)>0:
			print str(self.vehicle.id) + " SL " + str(len(EV_messages))

	def message_handler(self):
#function that receive the incoming messages. wait for v2v_network_input to be not empty, this should trigger some event?

		while True:
			if hasattr(self.vehicle, 'v2v_network_input'):
				if self.vehicle.v2v_network_input:
					self.message_incoming=self.vehicle.v2v_network_input
					self.vehicle.v2v_network_input=[]

			if hasattr(self.vehicle, 'v2v_wifi_input'):
				if self.vehicle.v2v_wifi_input:
					self.wifi_incoming=copy.deepcopy(self.vehicle.v2v_wifi_input)
					#self.parce_incoming_messages(self.wifi_incoming)
					self.vehicle.v2v_wifi_input=[]

			time.sleep(0.01)
				#filter the incoming list

			# #Handle the message: filter for importance
			# if self.vehicle.id==-5:
			# 	print self.message_incoming
			# end_time=time.time()
			# elapsed_time=end_time-start_time
			# sleep_time=0.04-elapsed_time
			# if sleep_time>0:
			# 	time.sleep(sleep_time)
			# 	self.communication_error=False
			# else:
			# 	self.communication_error=True

	def parce_incoming_messages(self,messages):
		#What cars are we interested in?
		# This will depend on what scenario we do.. feels unecessary to do more than necessary.

		##### SENARIO 1 ####
		#IF the car is forward most important object
			#states, intention, flags

		# IF the car is BWMO
			# Distance to car, STOM flag

		# are cars in fron of us forgemerging?
	
		#### SENARIO 2 ####
		#If the car

		for m in messages:
			if 'station_type' in m:
				if m['station_type']==10:
					if m['event_type']==95:
						self.last_emergency_message_timestamp = time.time()

	def is_emergency_vehicle_close(self):
		if self.last_emergency_message_timestamp==-1:
			return False
		elif time.time()-self.last_emergency_message_timestamp<2.0:
			return True
		return False

	def get_RSU(self):
		return self.vehicle.sml_world.rsu

	def update_emergency_flag(self):
		if self.i_am_stationary() == True:
			self.emergency_brake_flag = False

	def emergency_brake(self):
		self.set_CC_control(0)
		self.emergency_brake_flag = True

	def i_am_stationary(self):
		if self.get_velocity()<1/3.6:
			return True
		return False

	def release_emergency_brake(self):
		self.emergency_brake_flag = False

############################################

	# Function calld by the controller















################ needed for other parts of SML ##############

	def waitForEvent(self, event, function_to_run, abort_parameter, function_params = (), time_out = None):
		"""
		This function starts a thread that waits on event and then runs function_to_run with arguments function_params.

		:event: Thread waits for this event
		:function_to_run: Function to run when event is triggered
		:abort_parameter: Thread checks this to be False before running function_to_run
		:function_params: tuple or list of the parameters passed to function_to_run
		"""

		temp_thread = threading.Thread(target= self.waitForEventThread, args = (function_to_run, event, abort_parameter, function_params, time_out))
		temp_thread.daemon = True
		temp_thread.start()

	def waitForEventThread(self, function_to_run, event, abort_parameter, function_params, time_out):

		event.wait(time_out)

		if not abort_parameter:# and event.isSet():
			function_to_run(*function_params)
