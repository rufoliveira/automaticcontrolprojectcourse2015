import time, math, random, threading
import MessageModule

import numpy

import bodyclasses

from Constants import *
class RoadSideUnit(bodyclasses.RectangularObstacle):
	'''
	This Class implements the RSU
	'''

	def __init__(self, sml_world, RSU_id,merging_zone_distance):
		bodyclasses.RectangularObstacle.__init__(self)

		self.simulated = False
		self.detected = True

		self.sml_world = sml_world
		self.bodies_dict = self.sml_world.bodies_dict
		self.x_speed=0
		self.y_speed=0
		# The id of this vehicle
		self.id = RSU_id


		self.start_scenario = True
		self.end_of_scenario = False

		self.merging_zone_radius = merging_zone_distance

		# Definition of string constants
		self.RIGHT_LANE_STRING = "right_lane"
		self.CENTER_LANE_STRING = "center_lane"
		self.LEFT_LANE_STRING = "left_lane"
		
		self.road_module = sml_world.road_module

		# Initialization of the lane trajectorys
		self.traj = []
		self.right_lane_traj = []
		self.center_lane_traj = []
		self.left_lane_traj = []

		self.traj_points_per_meter = 5.
		# This fills self.<something>_lane_traj variables with information
		self.getLaneTrajectorys(self.road_module)

		# Old lane trajectory dict
		self.lane_traj_dict = {self.RIGHT_LANE_STRING: self.right_lane_traj, self.CENTER_LANE_STRING: self.center_lane_traj, self.LEFT_LANE_STRING: self.left_lane_traj}

		# numpy is used to find closest point
		self.np_lane_traj_dict = {self.RIGHT_LANE_STRING: self.np_right_lane_traj, self.CENTER_LANE_STRING: self.np_center_lane_traj, self.LEFT_LANE_STRING: self.np_left_lane_traj}


		self.traj = self.getLaneTrajectory(Lane.CENTER)


		# All input and output to and from the wifi and network broadcast system. As implemented now v2v_wifi_output and v2v_network_output is the outgoing messages from this vehicle.
		# They are also lists of at most one element (v2v_wifi_output and v2v_network_output).
		self.v2v_wifi_input = []
		self.v2v_wifi_output = []
		self.v2v_network_input = []
		self.v2v_network_output = []

		self.message_module=MessageModule.MessageModule(self, self)
		self.message_incoming=None
		self.wifi_incoming=None

		#initialize threads for communication
		self.start_thread(self.message_frequency,args=([10]))
		self.start_thread(self.message_frequency,args=([1]))
		self.start_thread(self.message_handler, args=([]))




	def set_trajectory_on_highway_lane(self, desired_lane):
		'''
		Given the name of a lane, it will find a closed trajectory
		on this lane. Specific for the GCDC Highway scenario.
		This trajectory becomes associated to the vehicle
		which will follow it.
		
		Inputs:
        desired_lane:
        	A string with the name of the desired lane, either 
    		"right", "center" or "left".
		'''

		if desired_lane == "right":

			right_lane_node_ids_list = self.sml_world.road_module.osm_node_tag_dict['right_lane_node']
			lane_node_id = right_lane_node_ids_list[0]

		elif desired_lane == "center":

			center_lane_node_ids_list = self.sml_world.road_module.osm_node_tag_dict['center_lane_node']
			lane_node_id = center_lane_node_ids_list[0]

		elif desired_lane == "left":

			left_lane_node_ids_list = self.sml_world.road_module.osm_node_tag_dict['left_lane_node']
			lane_node_id = left_lane_node_ids_list[0]

		else:

			raise NameError("Unexpected lane.")

		self.set_closed_trajectory_on_node(lane_node_id, 5.)

		return


	def set_closed_trajectory_on_node(self, traj_node_id, trajectory_points_per_meter = 5.):
		'''
		Given a node id, it will find a closed trajectory
		that includes it.
		This trajectory becomes associated to the vehicle
		which will follow it.
		The trajectory starts next to the given node.
		
		Inputs:
        traj_node_id:
            the id of the node where the trajectory
            should start
        trajectory_points_per_meter:
            the resolution of the trajectory
    	
		'''

		self.trajectory_points_per_meter = trajectory_points_per_meter
		[traj_x, traj_y] = self.sml_world.road_module.get_closed_path_from_node_id(traj_node_id, trajectory_points_per_meter)

		if len(traj_x) == 0 or len(traj_y) == 0:

			#Error, could not find a suitable trajectory
			raise NameError("Trajectory not found")

		traj_theta = []
		temp_theta = 0

		for idx in range( len ( traj_x ) - 1 ):

			delta_x = traj_x[idx+1] - traj_x[idx]
			delta_y = traj_y[idx+1] - traj_y[idx]

			temp_theta = math.atan2(delta_y, delta_x)
			traj_theta.append(temp_theta)
			
		traj_theta.append(temp_theta)

		if len(traj_y) != len(traj_x)  or len(traj_theta) != len(traj_x):

			raise NameError("DummyVehicle Trajectory creation resulted in a mistake!")

		self.traj = [traj_x, traj_y, traj_theta]
		self.np_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		return

	def set_vehicle_on_trajectory_state_fraction(self, fraction):
		'''
		Given a certain fraction, 0 <= fraction <= 1, this method
		will place the vehicle in said fraction of the trajectory.
		If fraction = 0, it will place at the beginning of the
		trajectory, if fraction = 1, at the end, if fraction = 0.5
		at the middle of the trajectory...

		Inputs:
		fraction:
			A float between 0 and 1, indicating the
			fraction of the trajectory where we wish
			to place the vehicle.
		'''

		if fraction < 0 or fraction > 1:

			raise NameError("Fraction not respecting the bounds")

		if len( self.traj ) == 0:

			raise NameError('DummyVehicle: trying to use set_vehicle_on_trajectory_state_fraction when trajectory is not yet defined')

		trajectory_idx = int( round ( fraction*len(self.traj[0]) - 1. ) )

		self.x = self.traj[0][trajectory_idx]
		self.y = self.traj[1][trajectory_idx]
		self.yaw = math.degrees( self.traj[2][trajectory_idx] )

		return



	def set_vehicle_on_trajectory_state(self, trajectory_idx = 0):
		'''
		Given a trajectory point index, it will place the car 
		on this trajectory point.
		Useful to place several cars on the same trajectory
		but in different places.

		Inputs:
		trajectory_idx:
			The trajectory point index, where we will
			place the car.

		'''

		self.current_trajectory_id = trajectory_idx

		if len( self.traj ) == 0:

			raise NameError('DummyVehicle: trying to use set_vehicle_on_trajectory_state when trajectory is not yet defined')

		self.x = self.traj[0][trajectory_idx]
		self.y = self.traj[1][trajectory_idx]
		self.yaw = math.degrees( self.traj[2][trajectory_idx] )

		return


	def find_closest_trajectory_point(self):
		'''
		This function will look for the closest point
		(in the trajectory) to the current state.

		Output:
		best_idx:
			The index of the closest trajectory point
			to the current car state
		'''

		np_state = numpy.array([[self.x],[self.y]])
		
		temp_distance = numpy.sum((self.np_traj[0:2, :] - np_state)**2, axis = 0)

		# Find the closest trajectory point that matches my desired speed and current heading
		best_idx = numpy.argmin(temp_distance)

		return best_idx

	def getLaneTrajectory(self, lane_traj_string):
		if self.lane_traj_dict:
			return self.lane_traj_dict[lane_traj_string]
		else:
			print "ERROR: no lane traj dict created"
			return None

	def getLaneTrajectorys(self, road_module):
		"""
		sets the lane trajectorys of the vehicle

		:osm_info: some kind of info
		:returns: boolean True if it was successful, has never happened that it failed without crashing though

		"""
		start_id = -1
		node_start = -1

		# right_lane_node_id = -1543
		# center_lane_node_id = -1424
		# left_lane_node_id = -1297

		# node_id_list = [ osm_info.osm_node_list[idx].id for idx in xrange( len( osm_info.osm_node_list ) ) ]

		# right_lane_node_start_idx = node_id_list.index(right_lane_node_id)		

		right_lane_node_ids_list = road_module.osm_node_tag_dict['right_lane_node']
		right_lane_node_id = right_lane_node_ids_list[0]

		center_lane_node_ids_list = road_module.osm_node_tag_dict['center_lane_node']
		center_lane_node_id = center_lane_node_ids_list[0]

		left_lane_node_ids_list = road_module.osm_node_tag_dict['left_lane_node']
		left_lane_node_id = left_lane_node_ids_list[0]


		[right_lane_traj_x, right_lane_traj_y] = road_module.get_closed_path_from_node_id(right_lane_node_id, self.traj_points_per_meter)
		[center_lane_traj_x, center_lane_traj_y] = road_module.get_closed_path_from_node_id(center_lane_node_id, self.traj_points_per_meter)
		[left_lane_traj_x, left_lane_traj_y] = road_module.get_closed_path_from_node_id(left_lane_node_id, self.traj_points_per_meter)

		if len(right_lane_traj_x) == 0 or len(center_lane_traj_x) == 0 or len(left_lane_traj_x) == 0 :
			#Error, could not find a suitable trajectory
			# Can happen if a vehicle is in a node for trucks only
			print "PlatooningManager Error: could not find a suitable trajectory"
			return False

		desired_velocity = 10

		# vehicle.set_state_trajectory(traj_x, traj_y, desired_velocity)
		self.set_right_lane_trajectory( right_lane_traj_x, right_lane_traj_y, desired_velocity )
		self.set_center_lane_trajectory( center_lane_traj_x, center_lane_traj_y, desired_velocity )
		self.set_left_lane_trajectory( left_lane_traj_x, left_lane_traj_y, desired_velocity )

		return True


	def set_right_lane_trajectory(self, traj_x, traj_y, desired_velocity):
		# Function used to define the right lane trajectory
		self.set_lane_trajectory( traj_x, traj_y, desired_velocity, self.RIGHT_LANE_STRING)

	def set_center_lane_trajectory(self, traj_x, traj_y, desired_velocity):
		# Function used to define the center lane trajectory
		self.set_lane_trajectory( traj_x, traj_y, desired_velocity, self.CENTER_LANE_STRING)

	def set_left_lane_trajectory(self, traj_x, traj_y, desired_velocity):
		# Function used to define the left lane trajectory
		self.set_lane_trajectory( traj_x, traj_y, desired_velocity, self.LEFT_LANE_STRING)

	def set_lane_trajectory(self, traj_x, traj_y, desired_velocity, lane_position):
		# Function used to set a lane trajectory, used for getting all the lane trajectorys
		
		traj_theta = []
		temp_theta = 0
		traj_time = []
		current_time = 0

		for idx in range( len ( traj_x ) - 1 ):

			delta_x = traj_x[idx+1] - traj_x[idx]
			delta_y = traj_y[idx+1] - traj_y[idx]

			temp_theta = math.atan2(delta_y, delta_x)
			traj_theta.append(temp_theta)

			traj_time.append(current_time)

			distance_moved = math.hypot(delta_x, delta_y) # Euclidean norm
			time_passed = distance_moved/desired_velocity

			current_time = current_time + time_passed

		traj_theta.append(temp_theta)
		traj_time.append(current_time)

		if len(traj_y) != len(traj_x) or len(traj_time) != len(traj_x) or len(traj_theta) != len(traj_x):

			raise NameError("SmartVehicle Trajectory creation resulted in a mistake!")

		if lane_position == self.RIGHT_LANE_STRING:

			self.right_lane_traj = [traj_x, traj_y, traj_theta, traj_time]
			self.np_right_lane_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		else:
		
			if lane_position == self.CENTER_LANE_STRING:

				self.center_lane_traj = [traj_x, traj_y, traj_theta, traj_time]
				self.np_center_lane_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

			else:

				if lane_position == self.LEFT_LANE_STRING:

					self.left_lane_traj = [traj_x, traj_y, traj_theta, traj_time]
					self.np_left_lane_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

				else:

					print "ERROR SmartVehicle: No suitable lane found!"

		return



	'''
	V2V Interface Methods
	Author: Rui
	These methods allow the V2VModule to interact
	with the SmartVehicle in a cleaner / more
	Object Oriented way
	'''
	def set_network_input_messages(self, messages_list):

		# Prints for Debugging	
		# print_str =  "Vehicle " + str(self.id) + " received the following network messages"
		# for message in messages_list:
		# 	print_str += "\n" + str(message)  
		# print print_str

		self.v2v_network_input.extend( messages_list )

		return

	def get_network_output_messages(self):

		# If no messages to output, return none
		if self.v2v_network_output==None:

			return None

		# Prints for Debugging	
		# print_str =  "Vehicle " + str(self.id) + " will output the following network messages"
		# for message in self.v2v_network_output:
		# 	print_str += "\n" + str(message)  
		# print print_str

		messages_to_output = self.v2v_network_output[:]

		del self.v2v_network_output[:]

		return messages_to_output

	def set_wifi_input_messages(self, messages_list):

		# Prints for Debugging	
		# print_str =  "Vehicle " + str(self.id) + " received the following Wi-Fi messages"
		# for message in messages_list:
		#  	print_str += "\n" + str(message)
		# print print_str

		self.v2v_wifi_input.extend( messages_list )

		#print "self.v2v_wifi_input length " + str(len(self.v2v_wifi_input))

		return


	def get_wifi_output_messages(self):

		# If no messages to output, return none
		if not self.v2v_wifi_output:

			return None

		# Prints for Debugging	
		# print_str =  "Vehicle " + str(self.id) + " will output the following Wi-Fi messages"
		# for message in self.v2v_wifi_output:
		# 	print_str += "\n" + str(message)  
		# print print_str

		messages_to_output = self.v2v_wifi_output[:]

		del self.v2v_wifi_output[:]

		return messages_to_output
############################################### Message functions

	def send_message(self,freq): #freq=1,25 or 10
		'''send message '''
		message_list=self.message_module.generateRSUMessage(freq)
		self.v2v_network_output.append(message_list)

	def message_frequency(self,freq):
		counter=1
		while True:
			start_time=time.time()
			if freq==10:
				self.send_message(10)
				time_end=time.time()
				elapsed_time=time_end-start_time
				sleep_time=0.1-elapsed_time
				if sleep_time>0:
					time.sleep(sleep_time)
					self.communication_error=False
				else:
					self.communication_error=True
			else:
				if counter==25:
					self.send_message(1)
					counter=1
				else:
					self.send_message(25)
					counter+=1
				time_end=time.time()
				elapsed_time=time_end-start_time
				sleep_time=0.04-elapsed_time
				if sleep_time>0:
					time.sleep(sleep_time)
					self.communication_error=False
				else:
					self.communication_error=True

	def parseWiFiMessages(self,messages):
		if messages is None:
			return 
		# print 'amount of messages ' + str(len(messages))
		EV_messages=[]
		for m in messages:
			if 'station_type' in m:
				if m['station_type']==10:
					if m['event_type']==95:
						EV_messages.append(m)
		if len(EV_messages)>0:
			print str(self.id) + " SL " + str(len(EV_messages))

	def message_handler(self):
#function that receive the incoming messages. wait for v2v_network_input to be not empty, this should trigger some event?

		while True:
			if hasattr(self, 'v2v_network_input'):
				if self.v2v_network_input:
					self.message_incoming=self.v2v_network_input
					self.v2v_network_input=[]

			if hasattr(self, 'v2v_wifi_input'):
				if self.v2v_wifi_input:
					self.wifi_incoming=copy.deepcopy(self.v2v_wifi_input)
					self.parce_incoming_messages(self.wifi_incoming)
					self.v2v_wifi_input=[]

			time.sleep(0.01)
				#filter the incoming list

			# #Handle the message: filter for importance
			# if self.vehicle.id==-5:
			# 	print self.message_incoming
			# end_time=time.time()
			# elapsed_time=end_time-start_time
			# sleep_time=0.04-elapsed_time
			# if sleep_time>0:
			# 	time.sleep(sleep_time)
			# 	self.communication_error=False
			# else:
			# 	self.communication_error=True

	def parce_incoming_messages(self,messages):
		#What cars are we interested in?
		# This will depend on what scenario we do.. feels unecessary to do more than necessary.

		##### SENARIO 1 ####
		#IF the car is forward most important object
			#states, intention, flags

		# IF the car is BWMO
			# Distance to car, STOM flag

		# are cars in fron of us forgemerging?
	
		#### SENARIO 2 ####
		#If the car

		for m in messages:
			if 'station_type' in m:
				if m['station_type']==10:
					if m['event_type']==95:
						self.last_emergency_message_timestamp = time.time()

	def is_emergency_vehicle_close(self):
		if self.last_emergency_message_timestamp==-1:
			return False
		elif time.time()-self.last_emergency_message_timestamp<2.0:
			return True
		return False



	def start_thread(self, handler, args=()):
		# Function that simplifies thread creation
		t = threading.Thread(target=handler, args=args)
		t.daemon = True
		# self.threads.append(t)
		t.start()


	def get_merging_zone_flag(self,vehicle):
		a = numpy.array((self.x,self.y))
		b = numpy.array((vehicle.x,vehicle.y))
		return numpy.linalg.norm(a-b)<self.merging_zone_radius