import ConfigParser
import shutil
from Constants import *
def init_config_file(filename):
	shutil.copyfile(filename,"current_configuration.ini")

def read_configuration_file():
	config = ConfigParser.ConfigParser()
	config.read("current_configuration.ini")

	Scenarios = {"Scenario 1":1,"Scenario 2":2,"Scenario 3":3}
	Intentions = {"straight":1,"left":2,"right":3}
	Lanes = {"right":Lane.RIGHT,"center":Lane.CENTER,"left":Lane.LEFT}

	L = dict(config.items("General"))
	L["scenario"] = Scenarios[L["scenario"]]
	L["intention1"] = Intentions[L["intention1"]]
	L["intention2"] = Intentions[L["intention2"]]
	L["intention3"] = Intentions[L["intention3"]]
	L["emergency_lane"] = Lanes[L["emergency_lane"]]
	L["vehicle_a_sizes"] = [int(s) for s in L["vehicle_a_sizes"].split(' ')]
	L["vehicle_b_sizes"] = [int(s) for s in L["vehicle_b_sizes"].split(' ')]

	for k,v in L.iteritems():
		if k in ["vehicle_a_sizes","vehicle_b_sizes"]:
			continue
		try:
			L[k] = int(v)
		except ValueError:
			try:
				L[k] = float(v)
			except ValueError:
				L[k] = v
	return L
