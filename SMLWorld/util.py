from threading import Thread, Lock
import copy
import collections
import Configuration
import time

mutex = Lock()

output = []

running = True


def is_running():
	global running
	return running
	
def stop():
	global running
	running = False

def gui_print(s):
	global output
	mutex.acquire()
	output.append(str(s))
	mutex.release()

def get_print():
	global output
	mutex.acquire()
	d = copy.deepcopy(output)
	output = []
	mutex.release()
	return d

trace_table = collections.OrderedDict()
trace_file = []
trace_activate = False
trace_time = time.time()

def begin_traces():
	global trace_file, trace_activate 
	conf = Configuration.read_configuration_file()
	trace_activate = conf["traces"]>0

	if trace_activate==False:
		return 

	print conf
	if conf["traces_filename"]!="":
		filename = "Traces/"+conf["traces_filename"]
	else:
		filename = "Traces/trace.txt"
	trace_file = open(filename,"w")

def add_to_trace(id,s):
	global trace_table, trace_activate
	_id = copy.copy(id)
	if isinstance(s,dict):
		_s = dict.copy(s)
	else:
		_s = copy.copy(s)
	if trace_activate==False:
		return 
	if _id in trace_table:
		flush_trace()
	trace_table[_id] = str(_s)

def flush_trace():
	global trace_file, trace_table, trace_activate,trace_time
	if trace_activate==False:
		return
	t = dict(trace_table)
	t["time"] = time.time()-trace_time
	trace_table = collections.OrderedDict()
	trace_file.write(str(dict(t))+"\n")

def end_traces():
	global trace_activate, trace_file
	if trace_activate==False:
		return 
	trace_file.close()

