#!/usr/bin/python

'''
This demo shows how to create and place dummy vehicles
in the scenario.
'''

import threading
import sys
# import os
import os.path
import time
# import math

sys.path.append('RoadModule')
import RoadModule

import SimulatorModule
import V2VModule
import UnityModule

import smartvehicle
import smartintersectionvehicle
import dummyvehicle

import VisualisationModule, VisualisationModuleSender

import multiprocessing

import MotionCaptureModule


# noinspection PyBroadException,PyArgumentList
class SMLWorld(object):
	"""SMLWorld project"""

	def __init__(self):
		super(SMLWorld, self).__init__()

		self.close = False

		self.qualisys_info = None

		self.close_sml_world = False

		# Boolean that defines if the simulation
		# has started or not.
		# If it has not, the V2VModule and 
		# SimulationModule will not be running.
		# The simulation can be started calling
		# the start_simulation() method.
		self.simulation_started = False

		# This is the dictionary which will have all of the bodies (real/simulated, smart/dummy, bus/persons, etc...) in the World
		self.bodies_dict = dict()


	def start_thread(self, handler, args=()):
		t = threading.Thread(target=handler, args=args)
		t.daemon = True
		t.start()

	
	
	def close_world(self):
		self.close_sml_world = True

	def create_dummy_vehicles(self):
		'''
		This method creates dummy vehicles and
		places them randomly on the HighwaySML 
		'''

		import random

		control_rate = 10.
		vehicle_id = -1

		num_dummy_vehicles = 20

		# The desired speed in km/h
		desired_speed_km_h = 40

		# List of the possible lanes to choose from
		possible_lanes = ["left", "center", "right"]

		for i in range(num_dummy_vehicles):

			# Creating the dummy vehicle
			temp_vehicle = dummyvehicle.DummyVehicle(self, vehicle_id, control_rate)

			desired_lane_idx = random.randint(0, 2)

			# Method specific for the highway scenario, allowing us
			# to place the car in a desired lane
			temp_vehicle.set_trajectory_on_highway_lane(possible_lanes[desired_lane_idx])

			traj_fraction = random.random()

			# Method to decide at which fraction of the trajectory the car
			# will be placed
			temp_vehicle.set_vehicle_on_trajectory_state_fraction(traj_fraction)

			# Setting the desired cruise speed for the vehicle
			temp_vehicle.set_cruise_speed(desired_speed_km_h/3.6)

			# Adding the vehicle to the dictionary of bodies
			self.bodies_dict[vehicle_id] = temp_vehicle

			# Starting the vehicle control loop thread
			temp_vehicle.start_control_loop()

			vehicle_id -= 1

		return

	def start_sml_world(self):
		'''
		Starting the SML Word
		'''

		file_location = "RoadModule/HighwaySML"

		# Create the road Module
		self.road_module = RoadModule.RoadModule(file_location)

		# Create the v2vModule
		self.v2v_module = V2VModule.V2VModule(self, v2v_wifi_range = 50.)

		# Create the SimulationModule
		self.simulator_module = SimulatorModule.SimulatorModule(self, simulation_rate = 50.)

		'''
		To start the Visualisation
		'''
		# Starting the VisualisationSenderModule
		self.visualisation_module = VisualisationModuleSender.VisualisationModuleSender(self, visualisation_address = '127.0.0.1')


		# display_width = 1400
		# display_height = 1050
		display_width = 800
		display_height = 600
		display_pixel_per_meter = 5

		# Set this Flag to True, otherwise it might cause bugs
		# Issue #12 Major Bug
		ground_projection = True

		args = (file_location, display_width, display_height, display_pixel_per_meter, ground_projection)

		self.visualisation_thread = multiprocessing.Process(target=VisualisationModule.VisualisationModule, args=args)

		# self.visualisation_thread = multiprocessing.Process(target=VisualisationModule.VisualisationModule, args=(file_location, 1100, 800, 7))
		# self.visualisation_thread = multiprocessing.Process(target=VisualisationModule.VisualisationModule, args=(file_location, 1920, 1200, 10))
		# self.visualisation_thread = multiprocessing.Process(target=VisualisationModule.VisualisationModule, args=(file_location, 1024, 768, -1 ,True))

		self.visualisation_thread.start()
		# self.visualisation_thread.join()

		'''
		Starting the interface with the Driver in the Loop
		'''
		# Create the UnityModule
		# If Unity running on the same computer
		# self.unity_module = UnityModule.UnityModule(self, unity_address = '127.0.0.1')
		# If Unity running on a different computer
		# self.unity_module = UnityModule.UnityModule(self, unity_address = '130.237.50.246')

		'''
		Starting the Motion Capture System
		'''
		#self.motion_capture_module = MotionCaptureModule.MotionCaptureModule(self)

		self.create_dummy_vehicles()

		self.start_simulation()

		return

	def quit(self):
		self.close_sml_world = True

	def start_simulation(self):

		self.simulation_started = True

		self.v2v_module.start_loop()
		self.simulator_module.start_loop()

		return


def main():

	smlworld = SMLWorld()

	print 'Object created successfully'

	smlworld.start_sml_world()

	smlworld.main_connected = True

	try:

		while not smlworld.close_sml_world:

			tic_main = time.time()

			toc_main = time.time()

			time.sleep(0.1)

	except KeyboardInterrupt:

		smlworld.quit()


if __name__ == "__main__":

	main()
