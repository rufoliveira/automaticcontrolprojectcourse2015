#__author__ = 'Carolina Eriksson'

'''Important! check so that all values exists in vheiclesupervisorymodule'''

import time
import copy
import time
from Constants import *
#needs to be fixed

class MessageModule:
	NOT_RECEIVED = "NOT_RECEIVED"
	
#instansiating a message class, that is used for creating each message that is to be sent
	def __init__(self, vehicle,vehiclesupervisorymodule):
		self.supervisory = vehiclesupervisorymodule
		self.vehicle = vehicle

	##### These functions are for broadcasting #####


	def generateMessage(self, frequency):
		""" This function generates a dict that contains all the values listed below. This is for broadcasting """
		# make sure all entries for GCDC communication are here

		#messages sent at 25 Hz
		message_dict = dict()
		message_dict['header']='header'																#id 1 header
		message_dict['time'] = time.time()							#might be part of the header?
		message_dict['message_id'] = hash(message_dict['time'])
		message_dict['vehicle_id'] = self.vehicle.id												#id 3 Station ID
		if self.vehicle.emergency_vehicle:
			message_dict['station_type']=10
		else:
			message_dict['station_type']=5																#id 4 unknown(0), pedestrian(1), cyclist(2),moped(3), motorcycle(4), passengerCar(5), bus(6), lightTruck(7),heavyTruck(8), trailer(9), specialVehicles(10), tram(11),


		message_dict['vehicle_length'] = self.vehicle.length										#id 6
		message_dict['vehicle_rear_axle_location']=0													#id 7
		message_dict['vehicle_width'] = self.vehicle.width											#id 8
		message_dict['controller_type']=self.supervisory.type_controller															#id 9
		message_dict['vehicle_response_time_constant']=0												#id 10
		message_dict['vehicle_response_time_delay']=0												#id 11
		message_dict['reference_position']=self.supervisory.position														#id 12
		if self.supervisory.position is not None:
			message_dict['x'] = self.supervisory.position[0]									#reference positions probably
			message_dict['y'] = self.supervisory.position[1]
		else:
			message_dict['x'] = None
			message_dict['y'] = None
		message_dict['heading']=self.supervisory.orientation																	#id 13
		message_dict['speed'] = self.supervisory.velocity									#id 14 speed =velocity
	#	message_dict['yaw_rate'] = self.vehicle.yaw																#id 15
		message_dict['long_vehicle_acc']=0															#id 16
		message_dict['desired_long_vehicle_acc']=0													#id 17
		message_dict['MIO_id']=self.supervisory.MIO_id										#id 18 Most Iportant Object in front needs to be defined in supervisorymodule
		message_dict['MIO_range']=self.supervisory.MIO_range								#id 19
		message_dict['MIO_bearing']=self.supervisory.MIO_bearing							#id 20
		message_dict['MIO_range_rate']=self.supervisory.MIO_range_rate						#id 21 velocity of MIO
		message_dict['time_headway']=self.supervisory.time_headway							#id 22 time to vehicle in front
		message_dict['cruise_speed']=self.supervisory.cruise_speed							#id 23 nominal speed of platoon
		message_dict['travelled_distance_CZ']=self.supervisory.travelled_CZ				#id 32
		message_dict['platooned_vehicle'] = self.vehicle.platooned_vehicle

		if frequency==1:
			#messages sent with 1Hz
			message_dict['vehicle_role']=0																#id 5 0 deafault, 6 emergency
			message_dict['merge_request_flag']=self.supervisory.merge_flag_request			#id 24
			message_dict['STOM']=self.supervisory.STOM										#id 25
			message_dict['merging_flag']=self.supervisory.merging_flag						#id 26
			message_dict['fwd_pair_partner'] = self.vehicle.fwd_pair_partner							#id 27
			message_dict['bwd_pair_partner'] = self.vehicle.bwd_pair_partner							#id 28
			message_dict['tail_vehicle_flag'] = self.vehicle.tail_vehicle					#id 29
			message_dict['head_vehicle_flag'] = self.vehicle.platoon_leader					#id 20
			message_dict['platoon_id'] = self.supervisory.platoon_id						#id 31
			message_dict['intention'] =self.supervisory.intention							#id 33
			message_dict['lane_entering_CZ'] = self.supervisory.lane_entering_CZ			#id 34
			message_dict['intersection_vehicle_counter'] = self.supervisory.intersection_vehicle_counter #id 35
			#message_dict['pair_acknowledge_flag'] = self.supervisory.pair_acknowledge_flag	#id 36 #currently not used in GCDC
			message_dict['participants_ready'] = True													#id 41 Not really used
			message_dict['start_scenario'] = self.supervisory.start_scenario														# id 42
			message_dict['EoS'] = False																	#id 43

		if frequency==10:
			message_dict['reference_time']=self.supervisory.reference_time						#id 37
			if self.vehicle.emergency_vehicle:
				self.vehicle.v2v_wifi_output=[]
				message_dict_wifi=dict()
				message_dict_wifi['time']=time.time()
				message_dict_wifi['station_type']=10
				message_dict_wifi['event_type']=95
				message_dict_wifi['lane_position']=self.supervisory.current_lane
				self.vehicle.v2v_wifi_output.append(message_dict_wifi)
			else:
				message_dict['event_type'] = self.supervisory.event_type							#id 38
			message_dict['closed_lanes'] = False															#id 39
			message_dict['lane_position'] = self.supervisory.current_lane															#id 40
		return message_dict


	def rsu_message(self, event):
		message_dict=dict()

		if event=='start_scenario':
			message_dict['start_scenario'] = True														# id 42
			message_dict['EoS'] = False

		if event=='end_Scenario':
			message_dict['start_scenario'] = False														# id 42
			message_dict['EoS'] = True

		return message_dict

	def generateRSUMessage(self,frequency):
		""" This function generates a dict that contains all the values listed below. This is for broadcasting """
		# make sure all entries for GCDC communication are here

		#messages sent at 25 Hz
		message_dict = dict()
		message_dict['header']='header'																#id 1 header
		message_dict['time'] = time.time()							#might be part of the header?
		message_dict['message_id'] = hash(message_dict['time'])
		message_dict['vehicle_id'] = self.vehicle.id												#id 3 Station ID
		message_dict['station_type']=15																#id 4 unknown(0), pedestrian(1), cyclist(2),moped(3), motorcycle(4), passengerCar(5), bus(6), lightTruck(7),heavyTruck(8), trailer(9), specialVehicles(10), tram(11),


		message_dict['vehicle_length'] = None #id 6
		message_dict['vehicle_rear_axle_location']=0													#id 7
		message_dict['vehicle_width'] = None #id 8
		message_dict['controller_type']=None #id 9
		message_dict['vehicle_response_time_constant']=0												#id 10
		message_dict['vehicle_response_time_delay']=0												#id 11
		message_dict['reference_position']=None #id 12
		message_dict['x'] = self.vehicle.y
		message_dict['y'] = self.vehicle.x
		message_dict['heading']=None #id 13
		#message_dict['speed'] = self.vehicle.bodies_dict[self.vehicle.id].commands['throttle']		#id 14 speed =velocity
	#	message_dict['yaw_rate'] = self.vehicle.yaw																#id 15
		message_dict['long_vehicle_acc']=0															#id 16
		message_dict['desired_long_vehicle_acc']=0													#id 17
		message_dict['MIO_id']=None #id 18 Most Iportant Object in front needs to be defined in supervisorymodule
		message_dict['MIO_range']=None #id 19
		message_dict['MIO_bearing']=None #id 20
		message_dict['MIO_range_rate']=None #id 21 velocity of MIO
		message_dict['time_headway']=None #id 22 time to vehicle in front
		message_dict['cruise_speed']=None #id 23 nominal speed of platoon
		message_dict['travelled_distance_CZ']=None #id 32
		message_dict['platooned_vehicle'] = None

		if frequency==1:
			#messages sent with 1Hz
			message_dict['vehicle_role']=0																#id 5 0 deafault, 6 emergency
			message_dict['merge_request_flag']=None #id 24
			message_dict['STOM']=None #id 25
			message_dict['merging_flag']=None #id 26
			message_dict['fwd_pair_partner'] = None #id 27
			message_dict['bwd_pair_partner'] = None #id 28
			message_dict['tail_vehicle_flag'] = None #id 29
			message_dict['head_vehicle_flag'] = None #id 20
			message_dict['platoon_id'] = None #id 31
			message_dict['intention'] =None #id 33
			message_dict['lane_entering_CZ'] = None #id 34
			message_dict['intersection_vehicle_counter'] = None #id 35
			#message_dict['pair_acknowledge_flag'] = self.supervisory.pair_acknowledge_flag	#id 36 #currently not used in GCDC
			message_dict['participants_ready'] = True													# id 41 Not really used
			message_dict['start_scenario'] = self.vehicle.start_scenario								# id 42
			message_dict['EoS'] = self.vehicle.end_of_scenario											# id 43

		if frequency==10:
			message_dict['reference_time']=time.time()													# id 37
			message_dict['event_type'] = "RoadWorks"													# id 38
			message_dict['closed_lanes'] = Lane.A														# id 39
			message_dict['lane_position'] = False														# id 40
		return message_dict

	# Paul: search in the dictionnary the messages
	def get_messages_from_vehicle(self,id):
		msgs = self.vehicle.supervisory_module.message_incoming
		for d in msgs:
			if "vehicle_id" in d.keys():
				if d["vehicle_id"]==id:
					return copy.deepcopy(d)