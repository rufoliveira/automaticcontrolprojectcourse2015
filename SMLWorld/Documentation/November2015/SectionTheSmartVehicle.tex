
\graphicspath{ {SectionTheSmartVehicle/Images/} }

\section{The Smart Vehicle}
\label{sec:the_smart_vehicle}

The Smart Vehicle is a vehicle developed by the 2015 Summer Students. The students involver were Elin Karlsson, Felix Abrahamsson, Johan Clemedson and Magnus Arvidsson. This vehicle had the task of solving some specific scenarios of the GCDC 2016 competition, specifically the Merging Scenario and the Emergency Vehicle Scenario.

Students Felix Abrahamsson and Magnus Arvidsson had the task of developing the Perception and Supervisory Modules. Their final code can be found in the \texttt{kth-smart-mobility-lab} repository, in the \texttt{IntegratingRealVehicles} branch. A useful documentation made by the summer students can be found in \cite{felix_magnus_report}.

Students Elin Karlsson and Johan Clemedson had the task of developing the Control Module for the Mini Trucks. Documentation of their work can be found in \cite{elin_johan_report}.

\subsection{Structure}

The Smart Vehicle follows the structure shown in figure \ref{fig:smart_vehicle_diagram}. Simply three different modules, running on their own threads are in charge of different functionalities.

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.8\textwidth]{smart_vehicle_diagram}
    \caption{The Smart Vehicle structure \label{fig:smart_vehicle_diagram} }
\end{figure}


\subsection{Perception Layer}

The Perception Layer can be found in the \texttt{vehicleperceptionmodule.py} file. Its main functions consist in understanding the surrounding vehicles, and generating easily understandable information about them, for the Supervisory and Control layers to use.

\subsection{Supervisory Layer}

The Supervisory Layer can be found in the \texttt{vehiclesupervisorymodule.py} file. It acts as the brain of the vehicle, setting the behaviour of the Control Layer, based on the information obtained from the Perception Layer.

\subsection{Control Layer}

The Control Layer can be found in the \texttt{vehiclecontrolmodule.py} file. The Control Layer implements the functionalities that allow the car to follow trajectories, keep velocities and keep distances to other cars. There car can be in several different modes of control, and they are switched on or off at the request of the Supervisory Layer.

\subsubsection{Cruise Control}

The simplest form of longitudinal control is the Cruise Control. When in this mode the Smart Vehicle will simply try to keep a certain reference velocity. To do so it simply uses a PID with an error input measured as $e = v_{ref} - v_{curr}$. The output is a throttle. This controller is implemented by the \texttt{get\_throttle\_CC} method.

\subsubsection{Cooperative Adaptive Cruise Control}

If the \texttt{ACCC} boolean attribute is set to \texttt{True}, then the Cooperative Adaptive Cruise Controller will be active. The objective of this controller is to keep a distance to another vehicle. To do so, we use the same controller as for the Cruise Controller, however the desired velocity that is fed into the cruise controller corresponds to the sum of the velocity of the vehicle to be followed, with an additional error term, resulting from the difference between the current distance to the vehicle to follow and the desired distance.

This will result in the car converging to a following distance equals to the desired distance, and moving at a speed equals to the followed car's speed.

\subsubsection{Lane Keeping}

The lateral controller is resposible for making the car follow the lanes of the highway. In the Highway scenario three lanes exist, the left, center and right lanes. The vehicle will always be following one of these lanes, or changing between them. In the current implementation, the car will follow the lane as defined in the SmartVehicle's attribute \texttt{desired\_lane}. See the \texttt{update\_lane} method for more details. 

The lateral controller is identical to the one used in the Dummy Vehicle case 
\ref{subsubsec:dummy_vehicle_lateral_control}.

\subsubsection{Lane Changing}

It is sometimes needed to changes between lanes. A simple way to do this would be to simply start tracking the trajectory of the lane we wish to move to. However this results in a big discontinuity in the trajectory points used as references to the controller, and the vehicle would simply jerk strongly to the desired lane.

In order to avoid this problem, and to make the lane changing more realistic, when a lane change is requested, the controller will compute an auxiliary trajectory that will take the car from one lane to the other in a smooth way. This functionality is implemented in the \texttt{smooth\_lane\_change\_numpy} method.

\subsubsection{Lane Offset}

In the emergency vehicle scenario the special functionality of driving with a certain lateral offset from the lane center is needed. This is used to make room for the emergency vehicle to pass between cars. To accomplish this a simple trick is implement in order to force the lateral controller to drive with a certain lateral offset.

The \texttt{lane\_offset} attribute is added to the lateral tracking error in the \texttt{\_get\_steering\_command} private method. This forces the car to drive with the \texttt{lane\_offset} lateral offset. To set \texttt{lane\_offset} one can make use of the setter method \texttt{set\_lateral\_offset}.

\subsection{Mini Truck Control Layer}

The Smart Vehicle can be run as a fully simulated vehicle, or making use of mini trucks.

\subsubsection{Structure}

To make use of the mini trucks a special Control Layer is implemented which sends the high level commands to a LabVIEW instance, which will in its turn broadcast this information to myRIOs that are controlling the mini trucks, as seen in figure \ref{fig:smart_vehicle_my_rio_structure}.

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.8\textwidth]{myRioDiagram}
    \caption{The Mini Truck Control Layer structure \label{fig:smart_vehicle_my_rio_structure} }
\end{figure}

From the Control Module to the myRIO Computer Module, the following information is sent:

\begin{description}
\item[time] The current time stamp, as obtained from the Qualisys System
\item[id] The id of the vehicle
\item[x] The x position of the vehicle, as obtained from the Qualisys System
\item[y] The y position of the vehicle, as obtained from the Qualisys System
\item[yaw] The yaw angle of the vehicle, as obtained from the Qualisys System
\item[velocity] The desired velocity for the vehicle, as computed by the Supervisory Layer
\item[current\_distance] The current distance to a vehicle to follow, as computed by the Perception Layer
\item[desired\_distance] The desired distance to a vehicle to follow, as computed by the Supervisory Layer
\item[lane] The desired lane for the vehicle to be in, as computed by the Supervisory Layer
\item[lane\_offset] The desired lateral offset from the lane center, as computed by the Supervisory Layer
\end{description}

This information is sent for each mini truck that is currently being used. The myRIO computer Module will then send this information to the appropriate myRIO. The myRIO will use this information to compute the low level control commands, velocity and steering voltages, in order to make the mini trucks have the desired behaviour.

\subsection{The myRIO Hardware}

The myRIO system controls the Mini Trucks through a radio controller. The radio controller board was obtained my dismouting the original Mini Truck controllers and rewiring the important components to the myRIO board.

\subsubsection{The Radio Board}

Figure \ref{fig:smart_vehicle_my_rio_and_board} shows the myRIO  connected to a Radio Controller board. A close up of the radio board in shown in figure \ref{fig:smart_vehicle_my_rio_board_close_up}, the wires and their respective functions are as follows: 

\begin{description}
\item[Brown] Electrical Ground
\item[Red] VCC (5V)
\item[Green] Trailer Hitch
\item[Orange] Steering Angle
\item[Yellow] Wheel Speed
\end{description}

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.8\textwidth]{myRioAndBoard}
    \caption{The myRIO connected to the radio board \label{fig:smart_vehicle_my_rio_and_board} }
\end{figure}


\begin{figure}[ht]
  \centering
    \includegraphics[width=0.8\textwidth]{myRioRadioBoard}
    \caption{Close-up of the radio board \label{fig:smart_vehicle_my_rio_board_close_up} }
\end{figure}

The Trailer Hitch angle can be connected directly to the same source as the VCC or the Ground. This will result in the hitch being fully open or fully closed.

Since we wish to control the Truck Speed and Steering Angle, we need to continuously change their voltages. An extensive study on the Mini Trucks behaviour according to the applied voltages is done in \cite{modelling_report_rui}. This study was done for the case where the radio boards are connected to a National Instruments Acquisition Board, if the radio boards are connected to the myRIO the voltage to steering and voltage to velocity relationships might change slightly. This happens due to different voltages in the VCC voltage.

\subsection{The myRIO Network}

The myRIO is intended to be used as a decentralized controller and being detached from a computer. However the myRIO still needs to communicate with the computer in order to get relevant information from the SML World.

The myRIOs are able to exchange information easily with a computer by being connected over a WiFi network. To do so we need to make sure that the myRIOs and the computer are on the same network, we achieve this by using the Cisco router at the lab.

Every computer in the lab is connected to the Internet through switches, however these switches only provide wired internet access, for the myRIOs we need to have  a wireless router. We use the Cisco router at the lab for that purpose. We simply connect the router's Internet Port to a switch (it is extremely important that the source of internet is connected to the Internet Port in the router), and the computer which will be communicating with the myRIOs to another port of the Router. A simple diagram of how to connect this system in shown in figure \ref{fig:my_rio_network_diagram}.

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.8\textwidth]{myRioNetworkDiagram}
    \caption{Network Diagram of the myRIO system \label{fig:my_rio_network_diagram} }
\end{figure}