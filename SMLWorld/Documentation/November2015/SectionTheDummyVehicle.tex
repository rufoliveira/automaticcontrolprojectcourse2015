
\section{The Dummy Vehicle}
\label{sec:the_dummy_vehicle}

The Dummy Vehicle is the simplest form of autonomous vehicle that is currently existing in the SML World. It can be used to simulate traffic, creating other road users and adding difficulty to the autonomous driving problem.

In this section the Dummy Vehicle is explained, and its inner workings are detailed. It is expected that the reader gets a better insight into the SML World after understanding how the Dummy Vehicle is implemented in it. The Dummy Vehicle can be found in the file \texttt{dummyvehicle.py}. 

\subsection{Features}

The dummy vehicle is able to follow a simple path of states $(x,y,\theta)$, or trajectory, at a determined velocity. If the path is closed, \textit{i.e.}, the end of the path is at the beginning of the path, the vehicle is able to loop forever on this path. 

The dummy vehicle is also able to detect surrounding vehicles, and brake/stop in case a collision is imminent. These two features allow the dummy vehicle to simulate somewhat realistic traffic behaviours.

\subsection{Creating a Dummy Vehicle}

This section will explain how a Dummy Vehicle can be created and set up to be used in the SML World. An example file, which shows this procedure can be found in \texttt{demo\_dummy\_vehicle\_placement.py}.

\subsubsection{Constructor}

The Dummy Vehicle constructor takes as arguments, the instance of the SML World, the vehicle id and an optional control rate.

The SML World instance is provided, so that the dummy vehicle can access the Road Module and make us of the trajectory generation methods that it provides. It is also used to kill the thread in case the SML World closes, this is achieved by polling the SML World's \texttt{close} attribute, at each control loop.

The vehicle id is the unique identifier that will be used as the key for the dictionary entry of this vehicle in the bodies dictionary of the SML World.

The control rate defines the frequency at which control inputs will be computed. The bigger it is, the better the controller performance, but the bigger the computational expenses. By default it is set to $10 Hz$. If there is a noticeable bad controller performance, it is advisable to try to increase this control rate, and check if it increases the performance.

\subsubsection{Defining the Trajectory}

After the vehicle creation, \textit{i.e.}, after calling the Dummy Vehicle constructor, one still has to define the trajectory it should perform. The trajectory is the path of $(x,y,\theta)$ states that the vehicle should follow.

To do so one can use the method \texttt{set\_closed\_trajectory\_on\_node}. This function takes as argument an OSM node id, and it will generate, if possible, a closed trajectory from it. For specifics check the implementation.

A more useful, and simpler, method that can be used is \texttt{set\_trajectory\_on\_highway\_lane}. This method is however specific for the Highway map. One simply has to provided a string with the desired highway lane (left, center or right), and the method will automatically set the vehicle trajectory.

When these methods are called, they will automatically generate the appropriate vehicle trajectory and store it in the attributes \texttt{traj}, a simple list of lists containing the trajectory states, and \texttt{np\_traj}, a numpy array which contains exactly the same information, but that allows for more efficient computations.

\subsubsection{Placing the Vehicle on the Trajectory}

By default a Dummy Vehicle is created with $(x,y,\theta) = (0, 0, 0)$, which is often not the desired initial state of the vehicle. To place the vehicle on a trajectory state, one can use the methods \texttt{set\_vehicle\_on\_trajectory\_state} or \texttt{set\_vehicle\_on\_trajectory\_state\_fraction}, which will set the vehicle's state $(x,y,\theta)$ such that it matches a certain part of the trajectory.

The \texttt{set\_vehicle\_on\_trajectory\_state} takes a trajectory index, and it will place the vehicle on the corresponding trajectory point. The \texttt{set\_vehicle\_on\_trajectory\_state\_fraction} takes a fraction between $0$ and $1$, and it will place the car at the corresponding fraction of the trajectory, \texttt{e.g.}, if one chooses a fraction of $0.5$, the car will be placed in the middle of the trajectory, this is, exactly between the start and the end.

\subsubsection{Defining the Cruise Speed}

The dummy vehicle has a desired speed which it will try to keep. This velocity can be set using the method \texttt{set\_cruise\_speed}, which takes as argument the desired speed in meters per second.

\subsubsection{Starting the Control Loop}

After creating the vehicle and setting its trajectory, one needs to add it to the bodies dictionary of the SML World. This will effectively add the vehicle to the SML World and start simulating it and making it interact with other vehicles.

The vehicle control loop then needs to be started, to do so, we simply call the \texttt{start\_control\_loop} method which will start the control loop thread.

\subsection{Controlling a Dummy Vehicle}

The Dummy Vehicle \texttt{control\_loop} is responsible for controlling the vehicle on the desired trajectory. This loop is running on its own thread, at the rate defined by the \texttt{control\_rate} attribute. If the control loop cannot run at the desired rate, usually due to not having enough computational power, it will issue a warning to the user via the terminal.

\subsubsection{Trajectory Tracking}

The trajectory tracking corresponds in making the vehicle converge to the trajectory. To do so, we first start by measuring the tracking error, the error between the current vehicle state and the desired trajectory state.

The desired trajectory state can be found using the \texttt{find\_closest\_trajectory\_point} method, which will find the closest trajectory state to the vehicle. The distance metric used is the euclidean distance, which compares the $(x,y)$ position of the vehicle with that of the trajectory state.

Once the closest trajectory state is found, the state error, a measurement of the error between the vehicle and the state, is computed, using the \texttt{get\_tracking\_error} method. The state error is composed of the longitudinal, the lateral and the orientation errors, which can then be used as inputs for the controller.

\subsubsection{Longitudinal Control}

The longitudinal controller does not actually require the state error in order to perform its task. The reason being that we are simply controlling the vehicle speed, and as such we only need to know the current vehicle speed and the desired speed in order to compute a throttle.

Currently, for simplifying purposes the Dummy Vehicle, can control its velocity by simply setting the attribute \texttt{commands['speed']}. By doing so, we can simplify a lot the processing, and still achieve a realistic behaviour.

If a more realistic model, one can switch to the acceleration based model. In this case the velocity is controlled making use of the attribute \texttt{commands['throttle']}. A simple PID takes as error input, the measurement $speed_{ref} - speed_{vehicle}$, and it will produce a control action corresponding to the throttle. For more implementation details see the \texttt{get\_throttle\_CC} method.

\subsubsection{Lateral Control}

\label{subsubsec:dummy_vehicle_lateral_control}

The lateral controller is responsible for steering the vehicle towards the trajectory. For that purpose it actuates over the \texttt{commands['steering']} attribute. The lateral controller uses the lateral error and orientation error to compute a suitable steering angle that will make the vehicle steer towards the trajectory.

As an implementation detail, for the lateral controller to work better, one usually chooses a trajectory point a bit further ahead of the closest trajectory point. This results in a controller with more look ahead and better performance. The lateral controller is implemented by the \texttt{get\_steering\_command} method.

\subsection{Obstacle/Collision Avoidance}

A big part of the autonomous driving is the avoidance of obstacles and collisions. The dummy vehicle is able to avoid collisions by braking/stopping when other vehicles become to close to it.

\subsubsection{Detecting Obstacles}

For the dummy vehicle case, the obstacles are simply the other vehicles on the road. The method \texttt{distance\_to\_car\_in\_future\_trajectory} simply computes the closest distance to another car, in the future trajectory. The dummy vehicle knows about other cars through its \texttt{sensor\_readings} attribute, which is set by the V2V Module.

The algorithm will go over the vehicle readings and initially perform the following checks:

\begin{description}
\item[distance check] If a vehicle is very far away, we ignore it and do not consider it as a possible danger
\item[direction check] If a vehicle is not in my frontal region, we ignore it and do not consider it as a possible danger
\end{description}

If the vehicles miss both of these checks they are then further checked, as they can represent possible collision dangers. The following check consists in iterating over the future trajectory, starting from the current closest trajectory point up to \texttt{distance\_to\_check} meters ahead. If any vehicle is at a distance smaller than \texttt{colliding\_distance} to any of the trajectory points being studied, then they are considered dangerous and the distance to them along the future trajectory is stored. The most dangerous vehicle will be the one which is considered dangerous and is the closest to the dummy vehicle. We call this distance the \texttt{closest\_car\_distance}.

This functionality is implemented in the \texttt{distance\_to\_car\_in\_future\_trajectory} method.

\subsubsection{Avoiding Collisions}

Once we get the \texttt{closest\_car\_distance}, we can effectively avoid collisions. To do so the simple algorithm is used:

\begin{algorithm}[H]

 \uIf{closest car distance $<$ full stop distance}{
 
	$desired speed = 0$
 	
 }
 \uElseIf{closest car distance $<$ safety distance}{
 
	$desired speed = cruise speed \times \frac{distance to car}{safety distance}$
 	
 }
 \Else{
 
	$desired speed = cruise speed$
 	
 }
 \caption{Adapting the velocity to other cars}
\end{algorithm}

This results in the vehicle having a smooth behaviour when approaching other vehicles from behind, and eventually coming to a stop if required.

\subsection{Interfacing with the other Modules}

As with other vehicles in the SML World, the Dummy Vehicle has to provide some interfacing functions for the Simulation Module.

\subsubsection{State Update}

The state update is the method that is called at each simulation step of the Simulation Module. This method is responsible for updating the state of the vehicle according to the simulation time, the current vehicle state and the current vehicle inputs.

This is done in the \texttt{vehicle\_state\_update} method, which is currently implementing a simple kinematic model.

\subsubsection{Sensors Update}

The sensors update is responsible for setting the most recent sensor readings. This is a method that is called at each simulation step of the Simulation Module, and is implemented in \texttt{set\_sensor\_readings}.