Documentation for the "V2V Communication Protocols for Platooning" summer
project can be found at the repository in:

IQmatic\message_protocols\old_merge_readme.txt
IQmatic\message_protocols\documentation

The code developed by the students is fully documented in comments.