import pygame
import time
import filterData
import sys

class GraphPlot:
	def __init__(self,size, scenario=1):
		graph_pic=pygame.image.load("graph"+str(scenario)+".png")
		s = graph_pic.get_rect().size
		w = size[0]
		h = size[1]
		t = min(1.0*w/s[0],1.0*h/s[1])
		ns = (int(s[0]*t),int(s[1]*t))

		self.graph_rescaled = pygame.transform.smoothscale(graph_pic, ns)
		if scenario==1:
			self.pos_states = [(0,0),(50, 102),(272, 107),(270, 260),(270, 437),(273, 636),(762, 634),(1250, 633),(755, 459),(452, 101),(669, 106),(862, 115),(1073, 106),(1291, 105),(1061, 257),(528, 298)]
		if scenario==2:
			self.pos_states = [(0,0),(31, 319),(244, 306),(402, 313),(591, 315),(1180, 319),(1163, 116),(584, 116),(1179, 744),(783, 626)]
		if scenario==3:
			self.pos_states = [(0,0),(52, 334),(194, 336),(305, 211),(307, 465),(561, 235),(863, 234),(465, 728)]


		self.myfont = pygame.font.SysFont("monospace", 15,bold=True)

		self.animate_t_start = 0.0
		self.is_moving = True

		self.state = {}
		self.next_state = {}
		self.T_fly = 0.2

	def get_car_pos(self,state):
		pos_dict = {}
		for i in set(state.values()):
			car_ss = [k for k,v in state.items() if v==i]
			nbr_car = len(car_ss)
			for idx,id_car in enumerate(car_ss):
				id_state = state[id_car]
				pos = self.pos_states[id_state]
				pos = (pos[0]+int((idx-(nbr_car-1)/2.0)*20),pos[1])
				pos_dict[id_car] = pos 
		return pos_dict

	def draw_car(self,screen,id_car,pos):
		pygame.draw.circle(screen, (255,0,0), pos, 10, 0)
		label = self.myfont.render(str(id_car), 1, (0,0,0))
		s = label.get_rect()
		screen.blit(label, (pos[0]-s.width/2,pos[1]-s.height/2))

	def draw_state(self,screen,t=0):
		screen.blit(self.graph_rescaled,(0,0))
		if t==0:
			pos_dict = self.get_car_pos(self.state)
			for id_car,pos in pos_dict.items():
				self.draw_car(screen,id_car,pos)
		else:
			pos_dict_1 = self.get_car_pos(self.state)
			pos_dict_2 = self.get_car_pos(self.next_state)
			for id_car in pos_dict_1.keys():
				p1 = pos_dict_1[id_car]
				p2 = pos_dict_2[id_car]
				x = int(p2[0]*t/self.T_fly+p1[0]*(self.T_fly-t)/self.T_fly)
				y = int(p2[1]*t/self.T_fly+p1[1]*(self.T_fly-t)/self.T_fly)
				pos = (x,y)
				self.draw_car(screen,id_car,pos)

	def draw(self,screen):
		if self.is_moving:
			t = time.time()-self.animate_t_start
			if t>self.T_fly:
				self.is_moving = False
				self.state = self.next_state
				self.draw_state(screen)
			else:
				self.draw_state(screen,t=t)
		else:
			self.draw_state(screen)

	def animate_until_next_state(self,next_state):
		if self.is_moving==True:
			self.state = self.next_state
		self.is_moving = True
		self.next_state = next_state
		self.animate_t_start = time.time()

	def set_state(self,state):
		self.is_moving = False
		self.state = state

def get_list_states(list_states):
	l = []
	l.append(list_states[0])
	for i in range(len(list_states)):
		if l[-1]!=list_states[i]:
			l.append(list_states[i])
	return l


def TraceGraph(states,scenario=1):
	pygame.init()
	w = 1400
	h = 800
	size=(w,h)
	screen = pygame.display.set_mode(size) 
	pygame.display.set_caption("Graph tracer")
	pygame.key.set_repeat(500,100)
	quit=False

	graph = GraphPlot(size,scenario=scenario)

	ids = 0
	graph.set_state(states[0])
	# render text
	while quit==False:
		screen.fill((255,255,255))
		graph.draw(screen)
		pygame.display.flip() # update the display

		ev = pygame.event.get()
		# proceed events
		for event in ev:
			# handle MOUSEBUTTONUP
			if event.type == pygame.MOUSEBUTTONUP:
				pos = pygame.mouse.get_pos()
				print pos

			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_RIGHT:
					if ids+1<len(states):
						ids+=1
					cars = states[ids]
					graph.animate_until_next_state(cars)
				if event.key == pygame.K_LEFT:
					if ids-1>=0:
						ids-=1
					cars = states[ids]
					graph.animate_until_next_state(cars)
				if event.key == pygame.K_ESCAPE:
					quit=True
			if event.type == pygame.QUIT:
				quit=True

		time.sleep(1.0/30.0)


if __name__ == "__main__":
	print "Scenario : "+str(sys.argv[1])
	print "Reading : "+str(sys.argv[2])
	scenario = int(sys.argv[1])

	vehicles=filterData.filter_Data(sys.argv[2])

	filterData.get_intersection_data(vehicles)
	states = filterData.get_states_vehicle(vehicles)

	print states
	TraceGraph(states,scenario=scenario)