import ast
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math
import numpy
import collections

def filter_Data(filename):
    trace_data=open(filename,'r+')
    lines=trace_data.readlines()
    vehicles_unsorted={}

    for i in lines:
        i=ast.literal_eval(i)# transform current line in a dictionary
        for k in i.keys(): # iterate through the keys of the dictionary, which are the vehicle numbers
            if k!='time':
                if k not in set(vehicles_unsorted.keys()): # check if the current vehicle has already a dictionary entry in vehicles
                    vehicles_unsorted[k]={}
                j=ast.literal_eval(i[k])
                for l in j.keys(): # iterate through all the keys of the vehicle and either append them to the previous ones or create a new entry in the dictionary
                    if l in vehicles_unsorted[k].keys():
                        vehicles_unsorted[k][l].append(j[l])
                    else:
                        vehicles_unsorted[k][l]=[j[l]]
                if 'time' in vehicles_unsorted[k].keys():
                    vehicles_unsorted[k]['time'].append(i['time'])
                else:
                    vehicles_unsorted[k]['time']=[i['time']]

    # sort keys
    vehicles=collections.OrderedDict()
    sorted_keys=sorted(vehicles_unsorted.keys(),reverse=True)
    for i in sorted_keys:
        vehicles[i]=dict(vehicles_unsorted[i])

    return vehicles


def plot_together(vehicles, key, title=None):
    # print everything in one plot except the position of the vehicles, which are plotted in subplots
    plt.figure()
    for k in vehicles.keys():
        if key=='VIVD' and k==-1:
            continue
        vehicle=vehicles[k]
        if len(numpy.shape(vehicle[key]))!=1: # considers only 2D matrices
            n=numpy.shape(vehicle[key])[1]
            for w in range(n):
                plt.subplot(n,1,w+1)
                plot_label='vehicle ' + str(int(math.fabs(k)))
                data=numpy.array(vehicle[key])
                plt.plot(vehicle['time'],data[:,w],label=plot_label)
                plt.legend(loc='upper right')
                plt.ylabel(key + '[' + str(w) + ']')
        else:
            plot_label='vehicle ' + str(int(math.fabs(k)))
            plt.plot(vehicle['time'],vehicle[key],label=plot_label)
            plt.ylabel(key)
            plt.legend(loc='upper left')
            plt.title(title)

    if key=='velocity':
        plt.plot([min(vehicle['time']),max(vehicle['time'])],[8.333,8.333],'b--')
    #plt.plot([min(vehicle['time']),max(vehicle['time'])],[13.889,13.889],'b--')
    #plt.plot([min(vehicle['time']),max(vehicle['time'])],[22.222,22.222],'b--')
    plt.xlabel('time [s]')

def plot_in_subplots(vehicles,key,title=None):
    # plot everything in subplots except the position of the vehicles use plot_together for that
    plt.figure()
    n=len(vehicles.keys())
    counter=1
    for k in vehicles.keys():
        if k!=0:
            vehicle=vehicles[k]
            plt.subplot(n,1,counter)
            if counter==1:
                if title is not None:
                    plt.title(title)
            #plot_label='vehicle ' + str(int(math.fabs(k)))
            plt.plot(vehicle['time'],vehicle[key])#,label=plot_label)
            plt.ylim(min(vehicle[key])-0.5,max(vehicle[key])+0.5)
            plt.legend(loc='upper left')
            plt.ylabel('vehicle ' + str(int(math.fabs(k))))
            plt.yticks(numpy.arange(min(vehicle[key]),max(vehicle[key])+1, 1.0))
            counter+=1
    plt.xlabel('time [s]')

def plot_xy(vehicles,key,D3=False):
    # can be used for plotting the position as y over x instead of both over time
    # the key to use is always the position
    plt.figure()
    if D3:
        ax = Axes3D(plt.gcf())
    for k in vehicles.keys():
        vehicle=vehicles[k]
        plot_label='vehicle ' + str(int(math.fabs(k)))
        data=numpy.array(vehicle[key])
        if D3:
            ax.plot(vehicle['time'],data[:,0],data[:,1],label=plot_label)
        else:
            plt.plot(data[:,0],data[:,1],label=plot_label)
        plt.legend(loc='lower right')
        plt.ylabel('y position')
        plt.xlabel('x position')

def plot_euclid_dist(vehicles):
    states1=numpy.array(vehicles[-1]['vehicle state'])
    states2=numpy.array(vehicles[-2]['vehicle state'])
    states3=numpy.array(vehicles[-3]['vehicle state'])
    dist12=[]
    dist13=[]
    t1=vehicles[-1]['time']
    t2=vehicles[-2]['time']
    t3=vehicles[-3]['time']
    cancel2=vehicles[-2]['cancel']
    cancel3=vehicles[-3]['cancel']
    #print str(cancel2)
    t12=sorted(list(set(t1) & set(t2)))
    t13=sorted(list(set(t1) & set(t3)))
    cancel_time2=t2[numpy.argmax(numpy.array(cancel2)>0)]
    cancel_index2=t12.index(cancel_time2)
    cancel_time3=t3[numpy.argmax(numpy.array(cancel3)>0)]
    cancel_index3=t12.index(cancel_time3)
    #print "index_cancel_time2 = " + str(t12.index(cancel_time2))
    #print "index_cancel_time3 = " + str(t13.index(cancel_time3))
    for i in t12:
        i1=t1.index(i)
        i2=t2.index(i)
        x1=states1[i1,0]
        y1=states1[i1,1]
        x2=states2[i2,0]
        y2=states2[i2,1]
        e12=math.sqrt(math.pow(x1-x2,2)+math.pow(y1-y2,2))
        dist12.append(e12)
    for i in t13:
        i1=t1.index(i)
        i3=t3.index(i)
        x1=states1[i1,0]
        y1=states1[i1,1]
        x3=states3[i3,0]
        y3=states3[i3,1]
        e13=math.sqrt(math.pow(x1-x3,2)+math.pow(y1-y3,2))
        dist13.append(e13)

    plt.figure()
    plt.plot(t12,dist12,'r',label='distance vehicle 1 to 2')
    plt.xlim(20,max(t12))
    plt.ylim(0,100)
    plt.plot(t13,dist13,'g',label='distance vehicle 1 to 3')
    plt.xlim(20,max(t13))
    plt.ylim(0,100)
    plt.plot([min(t12),max(t12)],[10,10],label='standstill distance')
    plt.plot(cancel_time2,dist12[cancel_index2],'*r')
    plt.plot(cancel_time3,dist13[cancel_index3],'*g')
    plt.xlabel('time [s]')
    plt.ylabel('euclidean distance [m]')
    plt.xlim(25,max(t13))
    plt.legend(loc='upper left')
    plt.title('euclidean distance between the vehicles')

vehicles=filter_Data('VIVD20')#_dist25_longest_time.txt')
scenario = 2# choose the scenario to filter the data

if scenario==1:
    plot_together(vehicles,'velocity')
    plot_together(vehicles,'hybrid state')
    plot_together(vehicles,'position')
    plot_in_subplots(vehicles,'abort')
    plt.show()
    print 'Tell me what to print!'
elif scenario==2:
    print str(vehicles[-1]['travelled distance'])
    #plot_xy(vehicles,'vehicle state')
    #plot_together(vehicles,'velocity')
    #plot_in_subplots(vehicles,'hybrid state')
    #plot_in_subplots(vehicles,'abort')
    plot_together(vehicles,'VIVD','VIVD')
    #plot_euclid_dist(vehicles)
    #plot_together(vehicles,'cancel')
    plt.show()
elif scenario==3:
    plot_together(vehicles,'velocity','vehicle velocities')
    # plot_xy(vehicles,'position')
    # plot_together(vehicles,'position')
    plot_in_subplots(vehicles,'hybrid state','states of the hybrid automaton')
    plot_in_subplots(vehicles,'abort','states of the abort automaton')
    plt.show()
print 'Done'
