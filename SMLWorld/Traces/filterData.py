import ast
import matplotlib.pyplot as plt
import math
import numpy
import collections
import plotGraph

def filter_Data(filename):
    trace_data=open(filename,'r+')
    lines=trace_data.readlines()
    vehicles_unsorted={}

    for i in lines:
        i=ast.literal_eval(i)# transform current line in a dictionary
        for k in i.keys(): # iterate through the keys of the dictionary, which are the vehicle numbers
            if k!='time':
                if k not in set(vehicles_unsorted.keys()): # check if the current vehicle has already a dictionary entry in vehicles
                    vehicles_unsorted[k]={}
                j=ast.literal_eval(i[k])
                for l in j.keys(): # iterate through all the keys of the vehicle and either append them to the previous ones or create a new entry in the dictionary
                    if l in vehicles_unsorted[k].keys():
                        vehicles_unsorted[k][l].append(j[l])
                    else:
                        vehicles_unsorted[k][l]=[j[l]]
                if 'time' in vehicles_unsorted[k].keys():
                    vehicles_unsorted[k]['time'].append(i['time'])
                else:
                    vehicles_unsorted[k]['time']=[i['time']]

    # sort keys
    vehicles=collections.OrderedDict()
    sorted_keys=sorted(vehicles_unsorted.keys(),reverse=True)
    for i in sorted_keys:
        vehicles[i]=dict(vehicles_unsorted[i])

    return vehicles


def get_intersection_data(vehicles):
    t = None
    for i in vehicles.keys():
        if t==None:
            t = set(vehicles[i]['time'])
        else:
            t.intersection_update(vehicles[i]['time'])

    for i in vehicles.keys():
        mask = []
        i = vehicles.keys()[0]
        for j in range(len(vehicles[i]['time'])):
            if vehicles[i]['time'][j] in t:
                mask.append(j)
        for l in vehicles[i].keys():
            vehicles[i][l] = [m for idm,m in enumerate(vehicles[i][l]) if idm in mask]
    return vehicles

def get_states_vehicle(vehicles):
    l = []
    idv = vehicles.keys()[0]
    for i in range(len(vehicles[idv]['time'])):
        d = {k:v['hybrid state'][i] for k,v in vehicles.items()}
        if not (l!=[] and l[-1]==d):
            l.append(d)

    return l

def plot_together(vehicles, key):
    # print everything in one plot except the position of the vehicles, which are plotted in subplots
    plt.figure()
    for k in vehicles.keys():
        vehicle=vehicles[k]
        if len(numpy.shape(vehicle[key]))!=1: # considers only 2D matrices
            n=numpy.shape(vehicle[key])[1]
            for w in range(n):
                plt.subplot(n,1,w+1)
                plot_label='vehicle ' + str(int(math.fabs(k)))
                data=numpy.array(vehicle[key])
                plt.plot(vehicle['time'],data[:,w],label=plot_label)
                plt.legend(loc='upper right')
                plt.ylabel(key + '[' + str(w) + ']')
        else:
            plot_label='vehicle ' + str(int(math.fabs(k)))
            plt.plot(vehicle['time'],vehicle[key],label=plot_label)
            plt.ylabel(key)
            plt.legend(loc='upper right')
    plt.xlabel('time [s]')

def plot_in_subplots(vehicles,key):
    # plot everything in subplots except the position of the vehicles use plot_together for that
    plt.figure()
    n=len(vehicles.keys())
    counter=1
    for k in vehicles.keys():
        vehicle=vehicles[k]
        plt.subplot(n,1,counter)
        plot_label='vehicle ' + str(int(math.fabs(k)))
        plt.plot(vehicle['time'],vehicle[key],label=plot_label)
        plt.ylabel(key)
        plt.ylim(min(vehicle[key])-0.5,max(vehicle[key])+0.5)
        plt.legend(loc='upper left')
        counter+=1
    plt.xlabel('time [s]')

def plot_xy(vehicles,key):
    # can be used for plotting the position as y over x instead of both over time
    # the key to use is always the position
    plt.figure()
    for k in vehicles.keys():
        vehicle=vehicles[k]
        plot_label='vehicle ' + str(int(math.fabs(k)))
        data=numpy.array(vehicle[key])
        plt.plot(data[:,0],data[:,1],label=plot_label)
        plt.legend(loc='lower right')
        plt.ylabel('y position')
        plt.xlabel('x position')



if False:
    scenario = 3# choose the scenario to filter the data

    if scenario==1:
        plot_together(vehicles,'velocity')
        plot_together(vehicles,'hybrid state')
        plot_together(vehicles,'position')
        plot_in_subplots(vehicles,'abort')
        plt.show()
        print 'Tell me what to print!'
    elif scenario==2:
        plot_together(vehicles,'vehicle state')
        plot_together(vehicles,'velocity')
        plot_in_subplots(vehicles,'hybrid state')
        plot_in_subplots(vehicles,'abort')
        plt.show()
    elif scenario==3:
        #plot_together(vehicles,'velocity')
        #plot_xy(vehicles,'position')
        plot_in_subplots(vehicles,'hybrid state')
        plot_in_subplots(vehicles,'abort')
        plt.show()
    print 'Done'
