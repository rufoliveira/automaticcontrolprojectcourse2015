
import math
import abort
import time

# david wrote this file
class SupervisorScenario2:
    # This file contains dummy/skeleton functions for the second scenario
    # attributes a vehicle needs for scenario 2: intention in {1,2,3}, lane in {1,2,3}, travelled_distance, state [x,y,alhpa]

    def __init__(self,data):
        self.sml_supervisor=data
        self.abort_automaton=abort.abort(self.sml_supervisor)
        self.sml_supervisor.set_desired_velocity(30./3.6) # sets desired velocity to 30 kph
        # variables needed
        self.current_state=1 # set the state to the first one in the automaton when initializing
        self.old_state=1 #used to return to the state the car was in before abort
        # intersection variables
        self.r_cz=100 # radius of competition zone
        self.w_p=8.1 # width of primary road in m
        self.w_s=8.1 # width of secondary road in m
        self.psi_cz=90 # angle between primary and secondary road (for scenario 2 90 degrees) in degree
        self.maximum_speed=30./3.6 # maximum speed in intersection in m/s

        # host variables
        self.host_state=None
        self.host_velocity=None
        self.host_intention=None
        self.host_lane=None
        self.host_travelled_distance=None
        self.platoon_id=None

        # target variables
        self.target_state=None
        self.target_intention=None
        self.target_lane=None
        self.target_travelled_distance=0
        self.target_id=None

        self.VIVD=0 # virtual intervehicle distance between the target and the host


        # #print "Starting to include reachCZ control"
        # DELETE ME LATER
        # self.uReachCZ=[]
        # reachCZ_file=open('reachCZcontrol.txt','r')
        # for i in reachCZ_file.readlines():
        #     self.uReachCZ.append(float(i))

        # auxiliary variables for execution of the scenario
        self.cancel_help=0
        self.cancel_flag=0
        self.epsilon=0.1
        self.timeout=0.0
        self.r_i=10 # stand still distance
        self.headway=1# 0.5 # headway
        self.TVA_done=False # variable for checking if TVA is done
        self.messages=None # variable for all incoming messages
        self.intersection_vehicle_counter=0
        self.time_to_reach_CZ=25
        self.start_time_of_state_3=None

        print 'Supervisor 2 created'


    def execute_scenario(self):
        # This function is for executing the whole scenario and setting flags
        self.processData()# process data function
        if not self.abort_automaton.check_safety([1,2,3,4,5,6,7,8,11]):# check general safety, which is important for all states
            self.check_transitions() # Check if the new received data leads to a transition or not
            # print 'current state = ' + str(self.current_state) + ' id: ' + str(self.sml_supervisor.vehicle.id)
        else:
            if self.current_state!=9:
                self.old_state=self.current_state
            self.current_state=9 # if there is a safety warning go to the abort state
        self.update_flags_variables()# set flags according to state and update variables (like travelled distance) maybe function called state update

        return True


    def processData(self):
        # function for processing the data received by the main supervisory function, which is extracting the necessary information
        # about the target vehicle its intention and so on to update the private variables of SupervisorScenario2
        # i.e. just extract messages with id for scenario 2

        # first get data about host
        self.host_state=self.sml_supervisor.get_position()
        #self.host_state[1]-=100 # scaling
        #print 'id = ' + str(self.sml_supervisor.vehicle.id) + ' and position = ' + str(self.host_state)
        self.host_state.append(self.sml_supervisor.get_orientation())
        if self.current_state<5:
            self.set_distance_to_intersection()
        self.host_velocity=self.sml_supervisor.get_velocity()

        self.host_lane=self.sml_supervisor.get_lane_entering_CZ()
        self.host_intention=self.sml_supervisor.get_intention()
        # process messages then to get the right target information
        # if self.target_id is None: # if TVA is not done we don't have any target and do not care about the message
        #     #print 'vehicle doesnt have any target: ' + str(self.sml_supervisor.vehicle.id)
        #     return
        # else: # get the target information by parsing the messages, when the message system is working
        self.messages=self.sml_supervisor.get_messages()
        # print self.messages
        target_information=self.parseMessages(self.messages)

        if target_information is None:
            #print "The target id is not contained in the received messages of vehicle " + str(self.sml_supervisor.vehicle.id)
            return
        else: # protects code from reading the last message in the received messages after running through the for loop
            #print 'checking messages vehicle id = ' + str(self.sml_supervisor.vehicle.id)
            x=target_information['reference_position'][0]
            y=target_information['reference_position'][1]
            #y-=100
            heading=target_information['heading']
            self.target_state=[x,y,heading]
            if 'intention' in target_information: # check if the intention and the lane are included in the message, if intention is included the lane should be there as well
                self.target_intention=target_information['intention']
                self.target_lane=target_information['lane_entering_CZ']
            if target_information['travelled_distance_CZ'] is not None:
                self.target_travelled_distance=target_information['travelled_distance_CZ']
            else:
                print 'vehicle ' + str(self.sml_supervisor.vehicle.id) + ' is too early'
                self.target_travelled_distance=0 # in case we enter the competition zone too early we set the travelled distance of the target to 0 to be able to run the scenario further
                # this might have to be changed
        return

    def parseMessages(self,messages):
        possible_target_messages=[]
        #print "amount of messages: " + str(len(messages))
        for m in messages: # parse all messages
            if 'intersection_vehicle_counter' in m: # if there is a message with an intersection vehicle counter...
                if self.intersection_vehicle_counter<m['intersection_vehicle_counter']: #...check if your counter is smaller than the one in the message...
                    self.intersection_vehicle_counter=m['intersection_vehicle_counter'] #...if yes save it as your counter

            if 'vehicle_id' in m: # if the message contains a vehicle id...
                if m['vehicle_id']==self.target_id: #... check if it is the vehicle id of your target...
                    possible_target_messages.append(m) #...and if yes, safe it as a possible message of your target

        if len(possible_target_messages)==0: # if there is no messages with the right target_id
            return None
        elif len(possible_target_messages)!=1: # check if we have more than one message from our target
            time_max=-1
            for m in possible_target_messages: # if yes choose the most current message
                if time_max<m['time']:
                    target_information=m
                    time_max=m['time']
        else:
            target_information=possible_target_messages[0] # otherwise choose the only possible message that is available

        return target_information



    def check_transitions(self):# this is maybe a more general function
        # according to the current state the transitions are checked, and the state is changed if a transition fires
        if self.current_state==1: # if you are in the idle state
            if not self.sml_supervisor.EoS:
                self.current_state=2
        elif self.current_state==2: # if you are in the waiting state...
                if self.sml_supervisor.has_simulation_started(): # ... check if the simulation is started
                    self.sml_supervisor.set_desired_velocity(30./3.6) # sets desired velocity to 30 kph
                    self.current_state=3 #... fire transition to the third state
                    #self.sml_supervisor.set_reachCZ_control(self.uReachCZ) # DELETE ME LATER
                    self.sml_supervisor.activate_reachCZ_control()
                    self.start_time_of_state_3=time.time()
        elif self.current_state==3: # reaching CZ state
            if time.time()-self.start_time_of_state_3>self.time_to_reach_CZ-0.02 and time.time()-self.start_time_of_state_3<self.time_to_reach_CZ+0.02:
                print 'here ' + str(self.host_state) + 'velocity: ' + str(self.host_velocity) + 'lane: ' + str(self.host_lane)
                self.abort_automaton.set_safety9()
                if not self.abort_automaton.check_safety([9]):
                    self.sml_supervisor.set_intersection_vehicle_counter(self.intersection_vehicle_counter)
                    self.sml_supervisor.deactivate_reachCZ_control()
                    self.current_state=4
                else:
                    self.old_state=self.current_state
                    self.current_state=9

        elif self.current_state==4: # TVA state
            if self.TVA_done:
                if self.host_lane!=1:
                    self.current_state=5
                else:
                    self.current_state=7


        elif self.current_state==5: # if you are in state 5 of scenario 2, which is the VCACC state
            if not self.abort_automaton.check_safety([10,12,13]):
                self.cancel_flag=self.cancel_VCACC()
                if self.cancel_flag==1:
                    self.current_state=8 # cancel to CACC
                elif self.cancel_flag==2:
                    self.sml_supervisor.set_CC_control(self.maximum_speed)
                    #self.sml_supervisor.vehicle.control_module.activate_mix_controller_VCACC_to_CC()
                    self.current_state=6 # cancel to CC
                else:
                    self.current_state=5 # stay in your state
            else:
                self.old_state=self.current_state
                self.current_state=9

        elif self.current_state==6: # CC mixing state
            self.current_state=7 # skip mixing state for now

        elif self.current_state==7: # CC state
            if self.sml_supervisor.get_EoS_flag():
                self.current_state=1
                self.sml_supervisor.set_CC_control(0)
            return

        elif self.current_state==8: # CACC state
            # is hopefully the end state
            if not self.abort_automaton.check_safety([12,13]):
                if self.sml_supervisor.get_EoS_flag():
                    self.current_state=1
                    self.sml_supervisor.set_CC_control(0)
                return
            else:
                self.old_state=self.current_state
                self.current_state=9

        elif self.current_state==9: # abort state
            if self.abort_automaton.state==1:
                self.current_state=self.old_state
            return

        else:
            return

    def update_flags_variables(self):
        # this function updates the flags and variables according to the state we are in
        if self.current_state==1: # idle state
            # wait for scenario data
            return
        elif self.current_state==2: # waiting for start signal state
            # wait for start signal
            return
        elif self.current_state==3: # reaching CZ_state
            # execute reaching CZ controller
            self.sml_supervisor.set_CC_control(self.maximum_speed)
            return
        elif self.current_state==4: # TVA state
            self.TVA_done=self.target_vehicle_assignment() # do TVA

        elif self.current_state==5: # VCACC state
            x,y=self.transform_coordinates()
            self.host_travelled_distance=self.calculate_scaled_travelled_distance(x,y) # calculate the scaled travelled distance...
            self.sml_supervisor.set_travelled_distance_in_CZ(self.host_travelled_distance)
            self.VIVD=self.target_travelled_distance-self.host_travelled_distance # ...use it with the sent travelled distance to calculate virtual inter vehicle distance and...

            self.sml_supervisor.set_virtual_distance(self.VIVD) # set current VIVD, should maybe done in
            self.sml_supervisor.set_VCACC_control(self.target_id,self.r_i+self.headway*self.host_velocity)# ... activate VCACC with that distance
            return

        elif self.current_state==6: # mixing VCACC with CC state
            x,y=self.transform_coordinates()
            self.host_travelled_distance=self.calculate_scaled_travelled_distance(x,y) # calculate the scaled travelled distance...
            #print 'travelled distance while CC = ' + str(self.host_travelled_distance)
            self.sml_supervisor.set_travelled_distance_in_CZ(self.host_travelled_distance)
            self.VIVD=self.target_travelled_distance-self.host_travelled_distance
            self.platoon_id=1
            return

        elif self.current_state==7: # CC state
            # activate CC and use your goal position as reference state
            x,y=self.transform_coordinates()
            self.host_travelled_distance=self.calculate_scaled_travelled_distance(x,y) # calculate the scaled travelled distance...
            #print 'travelled distance while CC = ' + str(self.host_travelled_distance)
            self.sml_supervisor.set_travelled_distance_in_CZ(self.host_travelled_distance)
            self.VIVD=self.target_travelled_distance-self.host_travelled_distance
            self.sml_supervisor.set_CC_control(self.maximum_speed)
            self.platoon_id=1
            return

        elif self.current_state==8: # CACC state
            # activate CACC and choose the right vehicle as target/forward partner
            self.sml_supervisor.set_CACC_control(self.target_id,self.r_i+self.headway*self.host_velocity)
            x,y=self.transform_coordinates()
            self.host_travelled_distance=self.calculate_scaled_travelled_distance(x,y) # calculate the scaled travelled distance...
            self.sml_supervisor.set_travelled_distance_in_CZ(self.host_travelled_distance)
            self.VIVD=self.target_travelled_distance-self.host_travelled_distance
            return

        elif self.current_state==9: # abort state
            #self.sml_supervisor.set_CC_control(0) # in case of abort brake and slow down to zero
            self.abort_automaton.check_transitions()
            return
        else:
            return


    def cancel_VCACC(self):
        # implement table 4.1 in D2.2 as if conditions to know which cancellation method should be used
        # after checking the right if condition, see if the cancel condition is fulfilled and if yes return true otherwise false
        # example for cancelling VCACC for vehicle 2, where we assume here that the coordinates are with respect to the IRF
        # returns 0, 1 or 2 which corresponds to dont cancel, cancel to CACC and cancel to CC, respectively

        # maybe use two different flags, like cancel_to_cacc and cancel_to_cc for distinguishing where to go after VCACC in automton we use a cancel flag
        if self.host_lane==2: # host is on lane 2
            if self.host_intention==1: # and the host goes straight
                if self.target_lane==1 and self.target_intention==2:
                    new_value=-1*math.sin(self.host_state[2]*math.pi/180)*(self.target_state[0]-self.host_state[0])+math.cos(self.host_state[2]*math.pi/180)*(self.target_state[1]-self.host_state[1])
                    #print 'value ' + str(new_value)
                    if new_value*self.cancel_help<0:
                        return 2 # cancel to CC
                    else:
                        self.cancel_help=new_value
                        return 0
                elif self.target_lane==1 and self.target_intention==3:
                    new_value=math.fabs(self.host_state[2]-self.target_state[2]) # calculate the difference between the orientation of host and target
                    if new_value<=self.epsilon: # if the orientation is below a threshold than switch to CACC
                        return 1 # cancel to CACC
                    else:
                        return 0

        # cancellation of vehicle 3
        elif self.host_lane==3:
            if self.host_intention==1:
                if self.target_lane==1 and self.target_intention==2:
                    new_value=math.fabs(self.host_state[2]-self.target_state[2]) # calculate the difference between the orientation of host and target
                    if new_value<=self.epsilon: # if the orientation is below a threshold than switch to CACC
                        return 1 # cancel to CACC
                    else:
                        return 0
                elif self.target_lane==1 and self.target_intention==3:
                    # in this case vehicle on lane 1 and 3 do not have overlapping trajectories, so we can directly cancel to CC
                    return 2 # cancel to CC



    def calculate_scaled_travelled_distance(self,x_k,y_k):
        # THIS FUNCTION SHOULD MAYBE BE OUTSOURCED TO THE CONTROL MODULE
        # This functions calculates the travelled distance inside the competition zone, the calculations for that can be found in
        # section 4.1.3-4.1.5 in D2.2. Problems could be that we need to scale the coordinates back to the vehicle coordinate system
        # intention {1,2,3}={straight,left,right}
        if self.host_intention==1: # for straight intention the travelled distance is equal to x_k in the fixed coordinate system where the car entered the CZ
            if self.host_lane==2:
                travelled_distance=x_k
            elif self.host_lane==3:
                travelled_distance=x_k
            scaled_travelled_distance=travelled_distance # for the straight trajectory no scaling is needed

        elif self.host_intention==2: # left turn
            if self.host_lane==1:
                w_o=self.w_s
                w_f=self.w_p
                psi_l=math.radians(self.psi_cz) # should be in radians
            elif self.host_lane==3:
                w_o=self.w_p
                w_f=self.w_s
                psi_l=math.pi-math.radians(self.psi_cz) # should be in radians
            r_l=0.75*w_f
            a_l=0.5*w_f/math.sin(psi_l)-(r_l-0.25*w_o)*math.cos(psi_l)/math.sin(psi_l)
            b_l=-0.5*w_f*math.cos(psi_l)/math.sin(psi_l)+(r_l-0.25*w_o)/math.sin(psi_l)
            c_l=r_l*psi_l
            d_o=self.r_cz-a_l
            d_f=self.r_cz-b_l
            d_l=math.sqrt(math.pow(x_k-r_l*math.sin(psi_l)-d_o,2)+math.pow(y_k-r_l*(1-math.cos(psi_l)),2))
            psi_l_xy=math.atan2(x_k-d_o,r_l-y_k)
            if x_k<=d_o:
                #print 'huh'
                travelled_distance=x_k
            elif x_k>d_o and y_k<=r_l*(1-math.cos(psi_l)):
                #print 'hah ' + str(x_k)
                travelled_distance=d_o+r_l*psi_l_xy
            elif x_k>d_o and y_k>r_l*(1-math.cos(psi_l)):
                #print 'heh'
                travelled_distance=d_o+r_l*psi_l+d_l
            # up to here the travelled distance is calculated and from now on we will scale

            if travelled_distance<=d_o:
                scaled_travelled_distance=travelled_distance # until we reach the turn the travelled distance does not need to be scaled
            elif travelled_distance>d_o and travelled_distance<=d_o+c_l:
                scaled_travelled_distance=d_o+(a_l+b_l)/c_l*(travelled_distance-d_o)
            elif travelled_distance>d_o+c_l:
                scaled_travelled_distance=travelled_distance+a_l+b_l-c_l

        elif self.host_intention==3: # right turn
            if self.host_lane==1:
                w_o=self.w_s
                w_f=self.w_p
                psi_r=math.pi-math.radians(self.psi_cz)
            elif self.host_lane==2:
                w_o=self.w_p
                w_f=self.w_s
                psi_r=math.radians(self.psi_cz)
            r_r=0.25*w_f
            a_r=0.5*w_f/math.sin(psi_r)-(r_r+0.25*w_o)*math.cos(psi_r)/math.sin(psi_r)
            b_r=-0.5*w_f*math.cos(psi_r)/math.sin(psi_r)+(r_r+0.25+w_o)/math.sin(psi_r)
            c_r=r_r*psi_r
            d_o=self.r_cz-a_r
            d_f=self.r_cz-b_r
            psi_r_xy=math.atan2(x_k-d_o,r_r+y_k)
            d_r=math.sqrt(math.pow(x_k-r_r*math.sin(psi_r)-d_o,2)+math.pow(y_k+r_r*(1-math.cos(psi_r)),2))

            if x_k<=d_o:
                travelled_distance=x_k

            elif x_k>d_o and y_k>-1*r_r*(1-math.cos(psi_r)):
                travelled_distance=d_o+psi_r_xy*r_r

            elif x_k>d_o and y_k<+-1*r_r*(1-math.cos(psi_r)):
                travelled_distance=d_o+r_r*psi_r+d_r

            # up to here the travelled distance is calculated and from now on we will scale it
            if travelled_distance<=d_o:
                scaled_travelled_distance=travelled_distance # until we reach the turn the travelled distance does not need to be scaled

            elif travelled_distance>d_o and travelled_distance<=d_o+c_r:
                scaled_travelled_distance=d_o+(a_r+b_r)/c_r*(travelled_distance-d_o)

            elif travelled_distance>d_o+c_r:
                scaled_travelled_distance=travelled_distance+a_r+b_r-c_r

        return scaled_travelled_distance


    def transform_coordinates(self):
        # THIS FUNCTION SHOULD MAYBE BE OUTSOURCED TO THE CONTROL MODULE
        # transforms coordinates to the fixed coordinate systems of each lane for checking the travelled distances according to GCDC deliverable D2.2
        if self.host_lane==1:
            x_transformed=self.host_state[1]+self.r_cz*math.sin(math.radians(self.psi_cz))-0.25*self.w_s*math.cos(math.radians(self.psi_cz))-100
            y_transformed=-1*self.host_state[0]+self.r_cz*math.cos(math.radians(self.psi_cz))+0.25*self.w_s*math.sin(math.radians(self.psi_cz))
            return x_transformed,y_transformed
        elif self.host_lane==2:
            x_transformed=self.host_state[0]+self.r_cz
            y_transformed=self.host_state[1]+self.w_p/4-100
            return x_transformed,y_transformed
        elif self.host_lane==3:
            x_transformed=self.r_cz-self.host_state[0]
            y_transformed=self.w_p/4-self.host_state[1]-100
            return x_transformed,y_transformed

    def target_vehicle_assignment(self):
        # this function can be used for the target vehicle assignment
        # up to now it is assigning the targets only according to the second scenario of GCDC
        if self.host_lane!=1:
            self.target_id=-1
            self.platoon_id=2 # maybe not necessary
        else:
            self.target_id=None
            self.platoon_id=1 # maybe not necessary
        self.intersection_vehicle_counter+=1
        print 'Intersection counter of vehicle ' + str(self.sml_supervisor.vehicle.id) + ': ' + str(self.intersection_vehicle_counter)

        return True


    def reached_CZ(self):
        # THIS FUNCTION SHOULD MAYBE BE OUTSOURCED TO THE CONTROL MODULE
        # This function is not used anymore
        # DELETE ME LATER, MAYBE
        # Calculates if we reached the competition zone and returns true or false
        if self.host_state is not None:
            x,y=self.transform_coordinates()
            if x>=-0.5: # should be changed to the tolerance value
                # print 'Vehicle ' + str(self.sml_supervisor.vehicle.id) + 'reached competition zone'
                self.intersection_vehicle_counter+=1
                print 'Intersection counter of vehicle ' + str(self.sml_supervisor.vehicle.id) + ': ' + str(self.intersection_vehicle_counter)
                print 'vehicle id: ' + str(self.sml_supervisor.vehicle.id) + ' vehicle position: ' + str(self.host_state) + ' velocity: ' +str(self.host_velocity)
                print 'length of reach CZ input: ' + str(len(self.sml_supervisor.vehicle.control_module.u_reachCZ))
                return True
        else:
            return False

        return False

    def set_distance_to_intersection(self):
        # This function sets the distance to the intersection according to the lane the vehicle is on
        if self.host_lane==1:
            self.sml_supervisor.set_distance_to_CZ(math.fabs(self.host_state[1]-100)) # if on lane 1, the distance is the absolute value of the y-coordinate
        else:
            self.sml_supervisor.set_distance_to_CZ(math.fabs(self.host_state[0])) # otherwise the distance is the absolute value of the x-coordinate

    def get_trace_data(self):
        # returns the data one wants to save in the trace file
        trace_data={'hybrid state':self.current_state,'vehicle state':self.host_state,'velocity':self.host_velocity,'abort':self.abort_automaton.state,'VIVD':self.VIVD,'cancel':self.cancel_flag}
        if self.host_travelled_distance is None:
            trace_data['travelled distance']=0
        else:
            trace_data['travelled distance']=self.host_travelled_distance

        return trace_data
