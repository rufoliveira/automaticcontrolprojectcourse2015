from Constants import *
import math
import time
import util
import copy
import abort

# TODO 
# Sometimes, A vehicles merge in front of the B leader
# A don't cancel pairing if B did it
# compare with uppaal
# clean up the code
# -1 fwd pair with itself A2B2_10_120_300 only happened once. probably not fixable


class SupervisorScenario1:

# last worked on by Carolina, 12/11
    def __init__(self,sml_supervisor):

        self.current_state=StateScen1.INIT # set the state to the first one in the automaton when initializing
        self.vehicle_supervisory_module=sml_supervisor
        self.abort=abort.abort(sml_supervisor)

        self.state = StateScen1.INIT
        self.ready = False
        self.t = time.time()
        self.message = None
        #Define flags (attributes) for the scenario
        self.HMI_init = True
        self.RSU_start_message = True
        self.OPC_merge_request = False
        self.B2A_bwd_flag = False
        self.we_are_leader = False
        self.fwd_merging_flag=True
        self.recieved_STOM = 0
        self.d = 0
        self.d_safe = 0
        self.mergeOK = 0
        self.EOS = False
        self.MIOL_fwd_flag = False
        self.fwd_p_merge_flag = False
        self.t_verif_pairing = 0
        self.tail_position = False
        self.RSU_merge_flag = False
        # Gap making safety distance
        self.gm_minimal_distance = 5.0
        self.gm_time_headaway = 1.0
        self.gm_rate_safety_distance = 0.95 

        # OA target id
        self.OA_target_id = None

        self.maximal_approach_distance_to_RSU = 40.0

        # run the safety check if the RSU is too close
        self.collision_RSU_safety_breaking = True

        self.scenario_velocity = 40/3.6


        self.safety_check_set = set([1,2,3,4,5,6,7,8,11,14,15,16,17])

        # Used to reset the scenario, we need some timers so that the other cars receive our informations
        self.restart_timer = None
        self.T_RESTART = 3.0

        self.last_pairing_update_time = None
        self.T_TIMEOUT_PAIRING_TRANSMISSION = 2.0

        #filename = "Traces/sup1_"+str(self.vehicle_supervisory_module.vehicle.id)+".txt"
        #self.trace = open(filename,"w")

    def execute_scenario(self): #Carolina
        # This function is for executing the whole scenario and setting flags
        self.processData()# process data function

        # Check if we need to execute the scenario or the abort module
        if self.abort.state!=1 or self.safety_check():
            self.abort.check_transitions()
            self.abort.abort_actions()
        else:
            self.update_controllers()
            # check safety, when abort automaton is working
            # print 'previous state = ' + str(self.current_state)  + ' id: ' + str(self.sml_supervisor.vehicle.id)
            self.check_transitions() # Check if the new received data leads to a transition or not
            # print 'current state = ' + str(self.current_state) + ' id: ' + str(self.sml_supervisor.vehicle.id)

            #self.update_flag_variables()# set flags according to state and update variables (like travelled distance) maybe function called state update
            # return the important flags, maybe they are already updated

            #self.write_traces()

    def processData(self): # // added by Carolina.
        # function for processing the data received by the main supervisory function, which is extracting the necessary information
        self.update_messages(self.vehicle_supervisory_module.message_incoming)

        # check for OPC_merge_request
        if time.time() - self.t > 5:
            self.OPC_merge_request = True
    # check B2A_bwd_pair_partner #should have the value for the partner ID
        if self.current_state==StateScen1.A_WAIT_PAIR:
            self.find_B2A_bwd_pair_partner()


        # check HMI_input, currently set to true in the beginning
        # check RSU_start_message currently set to true in the beginning

        # check if we are the leader of the platoon
        self.we_are_leader=self.check_leadership()
        self.vehicle_supervisory_module.set_leader_flag(self.we_are_leader)

        # Check if we are the tail vehicle

        # check if the car in front sent out merging flag
        
        # check self.recieved_STOM = 0
        # check self.d = 0
        self.MIOL_fwd_flag = self.vehicle_supervisory_module.do_i_have_a_miol_fwd()
        self.MIOL_fwd_id = self.vehicle_supervisory_module.get_moil_fwd()
        self.MIOL_fwd_not_paired = self.check_if_miol_is_not_paired(self.MIOL_fwd_id)
        self.pairupB_valid = self.check_valid_pairing(self.MIOL_fwd_id)

        self.bwd_STOM = self.get_bwd_STOM()
        
        self.fwd_p_merge_flag = self.has_partner_started_merging()
        # check mergeOK ?

        self.mergeOK = self.is_partner_merged()

        self.vehicle_supervisory_module.merging_flag = (self.current_state==StateScen1.A_MERGING)

        self.tail_merging_position = self.is_at_tail_merging_position()

        self.RSU_merge_flag = self.am_i_in_merging_zone()

        self.tail_position = self.is_at_tail_merging_position()

        # update OA controller
        self.update_OA()


    # This function defines the hybrid automata deciding the logics doe the scenario
    def check_transitions(self):
        # according to the current state the transitions are checked, and the state is changed if a transition fires
        if self.current_state==StateScen1.INIT:  #1 # inint state
            if self.HMI_init==True:
                self.goto(StateScen1.WAIT_RSU)

        elif self.current_state== StateScen1.WAIT_RSU:#2 # Waiting for the RSU start message

            if self.RSU_start_message== True: # getting the RSU message
                self.goto_start()

        # platoon A
        elif self.current_state==StateScen1.A_WAIT_OPC: #3 # platooning waiting for OPC_merge_request
            if self.OPC_merge_request == True:
                self.request_merging()
                self.goto(StateScen1.A_WAIT_PAIR)


        elif self.current_state==StateScen1.A_WAIT_PAIR: # 4# platooning waiting for B2A_bwd_pair
            if self.B2A_bwd_flag == True:
                self.pairupA()
                self.goto(StateScen1.A_WAIT_MERGE)

            if self.RSU_merge_flag==True and self.we_are_leader == True:
                if self.tail_position==True:
                    self.pairupA_tail()
                    self.gap_make()
                    self.goto(StateScen1.A_TAIL_GAP_MAKING)
                elif self.can_merge_at_tail()==True:
                    self.merge()
                    self.goto(StateScen1.A_MERGING)

        elif self.current_state == StateScen1.A_WAIT_MERGE: #5 # platooning waiting for our turn to be leader, and start merging
            if self.we_are_leader == True and self.fwd_merging_flag==True:
                self.gap_make()
                self.goto(StateScen1.A_WAIT_STOM)
            #elif:                       # we have to force merge because we are to close to the end of the merging zone
             #   self.current_state = 7

        elif self.current_state == StateScen1.A_WAIT_STOM: #6 # gapmaking, waiting for safty to merge
            if self.bwd_STOM == True and self.is_gap_made_A() and self.RSU_merge_flag:
                self.merge()
                self.goto(StateScen1.A_MERGING)
                # send merging command
            elif self.bwd_STOM==False and self.need_to_force_merge() and self.is_gap_made_A():
                self.force_merge()
                self.goto(StateScen1.A_MERGING)

        elif self.current_state == StateScen1.A_MERGING: # 7 # merging
            if self.vehicle_supervisory_module.am_i_merge():
                self.merge_done()
                self.reset_pairing()
                self.reset_OA()
                self.reset_safety_checks()
                self.goto(StateScen1.B_WAIT_FWD_PAIR)  # merge done, enering platoon B
                # make sure that the controllers are updatinga nd following the correct car.


        elif self.current_state == StateScen1.A_TAIL_GAP_MAKING: #8 # gapmaking, waiting for safty to merge
            if self.is_gap_made_A() and self.RSU_merge_flag:
                self.merge()
                self.goto(StateScen1.A_MERGING)
                # send merging command

        # platoon B
        elif self.current_state == StateScen1.B_WAIT_OPC:   #9
            if self.OPC_merge_request == True:
                self.release_merging()
                self.goto(StateScen1.B_WAIT_FWD_PAIR)

        elif self.current_state == StateScen1.B_WAIT_FWD_PAIR:  # 10     # in this state we look or a
            if self.EOS == True:
                self.goto(StateScen1.EOS)
            elif self.MIOL_fwd_flag == True :
                if self.MIOL_fwd_not_paired and self.pairupB_valid: # We have found a pairing partner to the left
                    self.pairupB()
                    self.gap_make()
                    self.t_verif_pairing = time.time()
                    self.goto(StateScen1.B_GAP_MAKING)
                # change flags so that we send out the id for our paired partner
                # start gapmaking controller

        # Check if the pairing has been accepted
        # It is a little bit more complex than jjust waiting: I need to check that actually the 
        elif self.current_state == StateScen1.B_WAIT_ACK_PAIRING:   #11
            if self.verify_fwd_pairing()==True:
                self.goto(StateScen1.B_GAP_MAKING)
            elif self.verify_fwd_pairing()==False:
                self.reset_scenario()

        elif self.current_state == StateScen1.B_GAP_MAKING: # 12# now we have a paired partner, start to gapmaking

            ## This condition is making it crash sometimes for no apparent reasons
            if self.MIOL_fwd_flag == None: ## this transistion is a bit weird.. check d>= d_Safe aswell
                self.reset_pairing()
                self.reset_OA()
                self.goto(StateScen1.B_WAIT_FWD_PAIR)
            elif self.is_gap_made_B():# and self.MIOL_fwd_flag != None:
                self.set_STOM(True)
                self.goto(StateScen1.B_A_IS_MERGING)
            elif self.fwd_p_merge_flag == True:
                self.mergeOK = 0
                self.goto(StateScen1.B_A_HAS_MERGED)

        elif self.current_state == StateScen1.B_A_IS_MERGING: # 13
            if self.mergeOK == 1:
                self.goto(StateScen1.B_A_HAS_MERGED)
                self.mergeOK = 0.

        elif self.current_state == StateScen1.B_A_HAS_MERGED: #14
            if self.is_partner_merged():
                self.reset_pairing()
                self.reset_messages()
                self.reset_OA()
                self.goto(StateScen1.B_WAIT_FWD_PAIR)

        elif self.current_state == StateScen1.WAIT_RESTART_SCENARIO:  #15  #state for restarting the scenario
            if time.time()-self.restart_timer>self.T_RESTART:
                self.goto_start()



    # checks the saftey criterias for the secenario, this is done
    def safety_check(self):
        return self.abort.check_safety(list(self.safety_check_set))

################################## SWITCHING FUNCTIONS ##########################################################

    # function used when switching between states
    def goto(self, next_state):
        if self.vehicle_supervisory_module.vehicle.id==-1:
            util.gui_print("{:<18}-> {:<18}".format(str(self.current_state),str(next_state)))
        self.current_state = next_state

    def goto_start(self):
        if self.vehicle_supervisory_module.platoon_id == 'A':                             # we are in platoon A
            self.vehicle_supervisory_module.merge_flag_request = True
            self.add_safety(abort.Safety.FWD_PAIR_EMERGENCY_BREAKING)
            self.goto(StateScen1.A_WAIT_OPC) #... fire transition to the third state

        elif self.vehicle_supervisory_module.platoon_id == 'B':                             #in platoon B
            self.goto(StateScen1.B_WAIT_OPC)

    def goto_restart(self):
        self.restart_timer = time.time()
        self.goto(StateScen1.WAIT_RESTART_SCENARIO)


################################ MERGING FUNCTIONS #################################################
    def force_merge(self):
        print "FORCE MERGE"
        self.remove_safety(abort.Safety.FWD_COLLISION_EMERGENCY_BREAKING)
        self.vehicle_supervisory_module.force_merge()

    def merge(self):
        self.vehicle_supervisory_module.merge()
        self.remove_safety(abort.Safety.FWD_COLLISION_EMERGENCY_BREAKING)

    #function that set the correct flags when the meging is done
    def merge_done(self):
        self.vehicle_supervisory_module.merge_flag_request = False
        self.vehicle_supervisory_module.merging_flag = False

    def is_partner_merged(self):
        msg = self.get_message(self.vehicle_supervisory_module.vehicle.fwd_pair_partner)
        if msg!=None:
            if 'merging_flag' in msg:
                return not (msg['merging_flag'] or msg['merge_request_flag'])
        return False

    def has_partner_started_merging(self):
        msg = self.get_message(self.vehicle_supervisory_module.vehicle.fwd_pair_partner)
        if msg!=None:
            if 'merging_flag' in msg:
                return msg['merging_flag'] or not msg['merge_request_flag']
        return False

    def is_at_tail_merging_position(self):
        if 'fr' in self.vehicle_supervisory_module.MIO_id:
            fid = self.vehicle_supervisory_module.MIO_id['fr']
            msg = self.get_message(fid)
            if msg!=None and 'tail_vehicle_flag' in msg:
                if msg['tail_vehicle_flag']==True:
                    return True
        return False

    def am_i_in_merging_zone(self):
        rsu = self.vehicle_supervisory_module.get_RSU()
        flag = rsu.get_merging_zone_flag(self.vehicle_supervisory_module.vehicle)              #we shoulden't do it like this
        return flag

    def need_to_force_merge(self):              #We shouldn't do it like this
        # get RSU distance
        id_RSU = self.vehicle_supervisory_module.get_RSU().id
        d_RSU = self.vehicle_supervisory_module.vehicle.perception_module.getLongitudinalDistanceToTarget(id_RSU)
        if d_RSU!=None and d_RSU<self.maximal_approach_distance_to_RSU:
            return True
        return False

    def request_merging(self):
        self.vehicle_supervisory_module.merge_flag_request = True

    def release_merging(self):
        self.vehicle_supervisory_module.merge_flag_request = False

    def can_merge_at_tail(self):
        if self.vehicle_supervisory_module.is_car_ready():
            if self.vehicle_supervisory_module.vehicle.perception_grid!=[]:
                if 'fr' in self.vehicle_supervisory_module.MIO_id:
                    moir_id = self.vehicle_supervisory_module.MIO_id['fr']

                    msg = self.get_message(moir_id)
                    if msg!=None:
                        if "tail_vehicle_flag" in msg:
                            if msg["tail_vehicle_flag"]==True:
                                return True
                if  not 'fr' in self.vehicle_supervisory_module.MIO_id and \
                    not 'r' in self.vehicle_supervisory_module.MIO_id and \
                    not 'br' in self.vehicle_supervisory_module.MIO_id:
                    return True
        return False


########################## Gap Making functions ###############################
    # Use to get the distance in gap making state
    # self.gm_minimal_distance : minimal safety distance
    # self.gm_time_headaway : minimal time between the next car and me
    def get_distance_to_gap_making(self):
        linear_speed = math.hypot(self.vehicle_supervisory_module.vehicle.x_speed, self.vehicle_supervisory_module.vehicle.y_speed)
        return self.gm_minimal_distance + self.gm_time_headaway*linear_speed

    def update_gap_making_safety_distance(self):
        safety_distance = self.get_distance_to_gap_making()
        self.vehicle_supervisory_module.set_desired_OA_distance(safety_distance)



    def is_gap_made_B(self):
        # Check for this distances: - host-fwd_pair -> d1
        #                           - host-fwd      -> d2
        #                           - fwd_pair-fwd  -> d3

        d = self.get_distance_to_gap_making()
        d = d * self.gm_rate_safety_distance

        fwd_id = self.get_fwd()
        fwd_p_id = self.vehicle_supervisory_module.vehicle.fwd_pair_partner
        if fwd_id!=None and fwd_p_id!=None:
            d1 = self.vehicle_supervisory_module.vehicle.perception_module.getLongitudinalDistanceToTarget(fwd_p_id)
            d2 = self.vehicle_supervisory_module.vehicle.perception_module.getLongitudinalDistanceToTarget(fwd_id)
            if d1==None or d2==None:
                return False
            my_length = self.vehicle_supervisory_module.vehicle.length
            length_fwd_p = float(self.get_message_variable(fwd_p_id,"vehicle_length"))

            d1 = d1-my_length
            d2 = d2-my_length
            d3 = d2-d1-length_fwd_p
            if d1>d and d2>d and d3>d:
                print [d1,d2,d3]
                return True
        return False

    def is_gap_made_A(self):
        d = self.get_distance_to_gap_making()
        fwd_p_id = self.vehicle_supervisory_module.vehicle.fwd_pair_partner
        if fwd_p_id!=None:
            d_fwd_p = self.vehicle_supervisory_module.vehicle.perception_module.getLongitudinalDistanceToTarget(fwd_p_id)
            d_fwd_p = d_fwd_p - self.vehicle_supervisory_module.vehicle.length
            if d_fwd_p>d*self.gm_rate_safety_distance:
                return True
        return False

    def gap_make(self):
        self.OA_target_id = self.vehicle_supervisory_module.vehicle.fwd_pair_partner



#######################################  PAIRING FUNCTIONS ##############################################################


    def find_B2A_bwd_pair_partner(self):
        '''
        We must accept the closest pairing in time (compare timestamps).
        '''
        timestamps = -1.0
        for i in range(len(self.message)):
            if 'fwd_pair_partner' in self.message[i]:
                msg=self.message[i]
                if msg['fwd_pair_partner']==self.vehicle_supervisory_module.vehicle.id:
                    if timestamps<msg['time']:
                        ID=msg['vehicle_id']
                        self.vehicle_supervisory_module.set_bwd_pair_partner(ID)
                        self.B2A_bwd_flag=True
                        timestamps = msg['time']
        if timestamps==-1:
            self.B2A_bwd_flag=False

    def verify_fwd_pairing(self):
        msg = self.get_message(self.vehicle_supervisory_module.vehicle.fwd_pair_partner)
        if msg!=None:
            if msg['bwd_pair_partner_time']-self.last_pairing_update_time<self.T_TIMEOUT_PAIRING_TRANSMISSION:
                #print "Wait bwd pairing information of "+str(self.vehicle_supervisory_module.vehicle.fwd_pair_partner)
                return None
            if msg['bwd_pair_partner']==self.vehicle_supervisory_module.vehicle.id:
                return True
            else:
                print "Pairing informations are inconsistents:\n\
                host pairing information fwd=%s\n\
                pair pairing information bwd=%s"%(str(self.vehicle_supervisory_module.vehicle.fwd_pair_partner),str(msg['bwd_pair_partner']))
                return False

        print "Pairing informations are not available!"
        return False

    def verify_bwd_pairing(self):
        msg = self.get_message(self.vehicle_supervisory_module.vehicle.bwd_pair_partner)
        if msg!=None:
            if not 'fwd_pair_partner_time' in msg:
                return None
            if msg['fwd_pair_partner_time']-self.last_pairing_update_time<self.T_TIMEOUT_PAIRING_TRANSMISSION:
                #print "Wait fwd pairing information of "+str(self.vehicle_supervisory_module.vehicle.bwd_pair_partner)
                return None
            if msg['fwd_pair_partner']==self.vehicle_supervisory_module.vehicle.id:
                return True
            else:
                print "Bwd pairing informations are inconsistents:\n\
                host pairing information bwd=%s\n\
                pair pairing information fwd=%s"%(str(self.vehicle_supervisory_module.vehicle.bwd_pair_partner),str(msg['fwd_pair_partner']))
                return False

        print "Pairing informations are not available!"
        return False


    def check_if_miol_is_not_paired(self,id):
        if id==False:
            return False
        for i in range(len(self.message)):
            msg=self.message[i]
            if 'vehicle_id' in msg and msg['vehicle_id']==id:
                if ('bwd_pair_partner' in msg) and (msg['bwd_pair_partner']==None):
                    return True
                else:
                    return False
        return None
    
    # TODO: Need to update the MIO, a car cannot be paired by 2 cars at the same time mainly because on car cannot be the MIOfwd object of 2 cars!
    def pairupB(self):
        self.last_pairing_update_time = time.time()
        self.vehicle_supervisory_module.vehicle.fwd_pair_partner = self.MIOL_fwd_id

    def pairupA(self):
        self.last_pairing_update_time = time.time()
        fid = self.get_fwd_bwd_pair()
        self.vehicle_supervisory_module.vehicle.fwd_pair_partner = fid

    def pairupA_tail(self):
        self.last_pairing_update_time = time.time()
        self.vehicle_supervisory_module.vehicle.fwd_pair_partner = self.vehicle_supervisory_module.MIO_id['fr']

    def get_fwd_bwd_pair(self):
        msg = self.get_message(self.vehicle_supervisory_module.vehicle.bwd_pair_partner)
        if msg==None or not 'MIO_id' in msg:
            return None
        mio = msg['MIO_id']
        if 'fm' in mio:
            return mio['fm']
        else:
            return None

    def reset_pairing(self):
        self.vehicle_supervisory_module.vehicle.fwd_pair_partner = None
        self.vehicle_supervisory_module.vehicle.bwd_pair_partner = None

    # Check if the potential pairing wanted is possible
    def check_valid_pairing(self, id):
        if self.vehicle_supervisory_module.is_vehicle_stopping(id) in [True,None]:
            return False
        return True


################################## MESSAGING FUNCTIONS #######################################################

    def update_messages(self,message_incoming):
        # Add the time to the messaging system:
        if self.message==None:
            self.message = copy.deepcopy(message_incoming)
        else:
            for msg in message_incoming:
                if 'vehicle_id' in msg:
                    i = self.get_message_index(msg['vehicle_id'])
                    if i!=None:
                        self.message[i].update(msg)
                        if 'bwd_pair_partner' in msg:
                            self.message[i]['bwd_pair_partner_time'] = msg['time']
                        if 'fwd_pair_partner' in msg:
                            self.message[i]['fwd_pair_partner_time'] = msg['time']



    def get_message(self,id):
        for i in range(len(self.message)):
            if 'vehicle_id' in self.message[i] and self.message[i]['vehicle_id']==id:
                return self.message[i]
        return None

    def get_message_index(self,id):
        for i in range(len(self.message)):
            if 'vehicle_id' in self.message[i] and self.message[i]['vehicle_id']==id:
                return i
        return None

    def get_message_variable(self,_id,_key):
        msg = self.get_message(_id)
        if msg!=None:
            if _key in msg:
                return msg[_key]
        return None


    def reset_messages(self):
        self.set_STOM(False)

################################### CONTROLLERS ######################################################

    def reset_OA(self):
        self.OA_target_id = None

    def update_controllers(self):
        if self.vehicle_supervisory_module.is_car_ready():
            if self.vehicle_supervisory_module.vehicle.perception_grid!=[]:
                if 'fm' in self.vehicle_supervisory_module.MIO_id:
                    id = self.vehicle_supervisory_module.MIO_id['fm']
                    if isinstance(id,int):
                        if id!=self.vehicle_supervisory_module.vehicle.control_module.ACCC_target_id and id!=1:
                            self.vehicle_supervisory_module.set_CACC_control(id,15.0)
                            self.ready = True



    def update_OA(self):
        target_vehicles = []
        if self.vehicle_supervisory_module.is_car_ready():
            if self.vehicle_supervisory_module.vehicle.perception_grid!=[]:
                if 'fm' in self.vehicle_supervisory_module.MIO_id:
                    id = self.vehicle_supervisory_module.MIO_id['fm']
                    if isinstance(id,int):
                        if id==1:
                            target_vehicles.append(id)

        if self.OA_target_id!=None:
            target_vehicles.append(self.OA_target_id)

        safety_distance = self.get_distance_to_gap_making()
        self.vehicle_supervisory_module.set_OA_control(target_vehicles,safety_distance)




#################################### GENERAL FLAGS ###################################################################
    def check_leadership(self):
        # if i have a fwd middle MIO, if he is on the same lane as me, then I am not the leader
        if 'fm' in self.vehicle_supervisory_module.MIO_id:
            ID_forward = self.vehicle_supervisory_module.MIO_id['fm']

            if self.vehicle_supervisory_module.get_RSU().id == ID_forward:
                return True

            msg = self.get_message(ID_forward)

            if msg!=None and 'platoon_id' in msg:
                platoon = msg['platoon_id']
                return platoon != self.vehicle_supervisory_module.platoon_id 
        else:
            return True
        return False

    def get_bwd_STOM(self):
        msg = self.get_message(self.vehicle_supervisory_module.vehicle.bwd_pair_partner)
        if msg!=None:
            if 'STOM' in msg:
                return msg['STOM']
        return False

    def set_STOM(self,v):
        self.vehicle_supervisory_module.STOM = v

    def get_fwd(self):
        if 'fm' in self.vehicle_supervisory_module.MIO_id:
            return self.vehicle_supervisory_module.MIO_id['fm']
        return None

    def reset_flags(self):
        self.B2A_bwd_flag = False

    def reset_scenario(self):
        print "Reset scneario car "+str(self.vehicle_supervisory_module.vehicle.id)
        self.reset_flags()
        self.reset_OA()
        self.reset_pairing()
        self.reset_safety_checks()
        self.reset_messages()
        self.goto_restart()

    def reset_safety_checks(self):
        self.add_safety(abort.Safety.FWD_COLLISION_EMERGENCY_BREAKING)
        self.remove_safety(abort.Safety.BWD_PAIRING_INFORMATION_CONSISTENCY)

########################################################################################################

    def deepish_copy(self,org):
        '''
        much, much faster than deepcopy, for a dict of the simple python types.
        '''
        out = dict().fromkeys(org)
        for k,v in org.iteritems():
            try:
                out[k] = v.copy()   # dicts, sets
            except AttributeError:
                try:
                    out[k] = v[:]   # lists, tuples, strings, unicode
                except TypeError:
                    out[k] = v      # ints
        return out

#Function that creates a dictionary with information that we we want to save from running an event.
    def get_trace_data(self):
        trace_data={'hybrid state':self.current_state[1],'abort':self.abort.state, "velocity":self.vehicle_supervisory_module.velocity,
                    'position':self.vehicle_supervisory_module.position, 'platoon':self.vehicle_supervisory_module.platoon_id}
        return trace_data

    def add_safety(self,id_safety):
        try:
            self.abort.disabled_safety_check_set.remove(id_safety)
        except KeyError:
            pass
        self.safety_check_set.add(id_safety)

    def remove_safety(self,id_safety):
        try:
            self.safety_check_set.remove(id_safety)
        except KeyError:
            pass
        self.abort.disabled_safety_check_set.add(id_safety)

