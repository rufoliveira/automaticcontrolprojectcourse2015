import math
import time
import abort
import util

# Emergency Vehicle Scenario according to GCDC
# written by Lars Lindemann
class SupervisorScenario3:

    def __init__(self,data):
        self.before_abort_state=0
        self.current_state=1 # set the state to the first one in the automaton when initializing

        # initialize abort module and vehicle supervisory module
        self.vehicle_supervisory_module=data
        self.abort=abort.abort(self.vehicle_supervisory_module)

        self.time_ev_leaving=None
        self.message_incoming = None
        self.wifi_incoming = None
        self.platoon_leader = None
        self.lane = None
        self.EV_lane = None
        self.control_set = 0
        self.EV_abort=False

        # desired platoon distance and lane offset for making gaps and time after EV left the wifi radius towards a car
        self.desired_distance = 25
        self.lane_offset = 2.5
        self.dead_time = 2

        #gather flags here
        self.printed = False
        self.emergency_flag=self.vehicle_supervisory_module.vehicle.emergency_vehicle # sets a flag to true if we have an emergency vehicle
        if self.emergency_flag:
            self.vehicle_supervisory_module.set_desired_velocity(80./3.6)
        else:
            self.vehicle_supervisory_module.set_desired_velocity(50./3.6)


    def execute_scenario(self):
        # This function is for executing the whole scenario and setting flags
        # process data function
        self.processData()
        # check transitions and run the Hybrid Automata
        self.check_transitions() # Check if the new received data leads to a transition or not

        return True


    def processData(self):
        # function for processing the data received by the main supervisory function, which is extracting the necessary information
        # all necessary data

        # wifi and network messages
        self.message_incoming = self.vehicle_supervisory_module.message_incoming
        self.wifi_incoming = self.vehicle_supervisory_module.wifi_incoming


        # several other information, that might change during run-time
        self.platoon_leader = self.vehicle_supervisory_module.vehicle.platoon_leader
        self.lane = self.vehicle_supervisory_module.current_lane
        self.id = self.vehicle_supervisory_module.vehicle.id
        self.scenario = self.vehicle_supervisory_module.GCDC_module.scenario
        self.desired_velocity = self.vehicle_supervisory_module.vehicle.desired_velocity

        return True


    def check_transitions(self):# this is maybe a more general function
        # according to the current state the transitions are checked, and the state is changed if a transition fires
        if self.emergency_flag==True:
            if not self.abort.check_safety([1,3,4,12]):#,13]) and not self.EV_abort:
                if self.control_set is 0 and not self.EV_abort:
                    self.vehicle_supervisory_module.set_CC_control(self.desired_velocity)
                    self.control_set=1
            elif self.EV_abort: #Abort state for EV
                self.abort.check_transitions()
                if self.abort.state==1:
                    self.EV_abort=False
            else:
                self.EV_abort=True

            return True
        else:
            if self.current_state==1:

                if self.abort.check_safety([1337]): #  dummy safety check
                    self.before_abort_state=self.current_state
                    self.change_state(7)
                else:
                    if not self.control_set: self.set_control(50)
                    if self.scenario==3: self.change_state(2)
                    return True

            elif self.current_state==2:

                if self.abort.check_safety([1,3,4,6,7,8,11,13]):#,13]):# no safety 12 here because vehicles are too close in the beginning
                    self.before_abort_state=self.current_state
                    self.change_state(7)
                else:
                    if not self.control_set: self.set_control(50)
                    if self.check_start_scenario()==True:
                        if self.platoon_leader==True: self.change_state(3)
                        else: self.change_state(4)
                    return True

            elif self.current_state==3:
                if self.abort.check_safety([1,3,4,6,7,8,11,12,13]):#,13]):
                    self.before_abort_state=self.current_state
                    self.change_state(7)
                else:
                    if not self.control_set: self.set_control(50)
                    if self.check_ev_approaching()==True: self.change_state(5)
                    if self.check_EoS()==True:
                        util.gui_print('Car '+str(self.id)+': EoS')
                        self.change_state(1)
                    return True

            elif self.current_state==4:
                if self.abort.check_safety([1,3,4,6,7,8,11,12,13]):#,13]):
                    self.before_abort_state=self.current_state
                    self.change_state(7)
                else:
                    if not self.control_set: self.set_control(50)
                    #self.abort.set_safety13()
                    self.abort.check_safety([11])
                    if self.check_ev_approaching()==True: self.change_state(5)
                    if self.check_EoS()==True:
                        util.gui_print('Car '+str(self.id)+': EoS')
                        self.change_state(1)
                    return True

            elif self.current_state==5:
                if self.abort.check_safety([1,3,4,6,7,8,11,12,13]):#,13]):
                    self.before_abort_state=self.current_state
                    self.change_state(7)
                else:
                    if not self.control_set: #it's important to notice that during the overtaking, only CC controller are used in all cars (according to GCDC)
                        if self.lane=='left_lane':
                            self.vehicle_supervisory_module.vehicle.setDesiredVelocity(40./3.6)#*0.925)
                            self.vehicle_supervisory_module.set_CC_control(self.vehicle_supervisory_module.vehicle.desired_velocity)
                        else:
                            self.vehicle_supervisory_module.vehicle.setDesiredVelocity(40./3.6)
                            self.vehicle_supervisory_module.set_CC_control(self.vehicle_supervisory_module.vehicle.desired_velocity)

                    #if self.check_ev_approaching_onlyEV(): self.send_EV_message_cars()
                    if self.printed==False: #executed only once
                        self.send_EV_message_cars()
                        if self.avoid_emergency_vehicle()==True: self.printed=True

                    if self.check_ev_leaving():
                        if self.platoon_leader==True:
                            self.return_back()
                            self.change_state(3)
                        else:
                            self.return_back()
                            self.change_state(4)

                        self.printed=False

                        return True

                    return False

            # neglected state so far
            elif self.current_state==6:
                #self.abort.check_safety([1,2,4])
                return True

            #abort state
            elif self.current_state==7:
                self.abort.check_transitions()
                if self.abort.state==1:
                    self.change_state(self.before_abort_state)

                return True

    # Checks for the latest EV message from an emergency vehicle and decides when the EV disappeared
    def check_ev_leaving(self):
        if self.wifi_incoming is not None:
            for i in self.wifi_incoming:
                if 'station_type' in i and 'event_type' in i:
                    if i['station_type']==10 and i['event_type']==95:
                        self.time_ev_leaving=i['time']

        if self.time_ev_leaving is not None:
            if time.time()-self.time_ev_leaving>self.dead_time:
                self.time_ev_leaving=None
                return True
            else: return False

        return False

    # Checks if an Emergency messages arrived either from and emergency vehicle or from another car (forwarded)
    def check_ev_approaching(self):
        if self.wifi_incoming is not None:
            for i in self.wifi_incoming:
                #emergency message check
                if 'station_type' in i and 'event_type' in i:
                    if i['station_type']==10 and i['event_type']==95 and time.time()-i['time']<2:
                        self.EV_lane = i['lane_position']
                        self.time_ev_leaving=time.time()#safe when message arrived
                        return True
                    #just a forwarded emergency message from another car
                    elif i['station_type']==5 and i['event_type']==95:
                        if i['time'] is not None:
                            if time.time()-i['time']<self.dead_time:
                                return True
        return False

    def check_EoS(self):
        for i in range(len(self.message_incoming)):
            if 'EoS' in self.message_incoming[i]:
                if self.message_incoming[i]['EoS']==True: return True

    def check_start_scenario(self):
        for i in self.message_incoming:
            if 'start_scenario' in i:
                if i['start_scenario']==True: return True

    # this functions prints driving commands on the HMI according to GCDC
    def avoid_emergency_vehicle(self):
        #in D3.2: lane 1==right(outermost), lane2==left, lane0=hard shoulder to the right
        #in SML: 'right_lane'==hard shoulder(D3.2), 'center_lane' == right lane(D3.2), 'left_lane'==left lane(D3.2)
        for i in self.wifi_incoming:
            if 'event_type' in i and 'lane_position' in i:
                if i['event_type']==95 and self.lane=='left_lane':
                        if i['lane_position']=='right_lane':
                            self.vehicle_supervisory_module.vehicle.control_module.set_lateral_offset(self.lane_offset)
                            util.gui_print('Car '+str(self.id)+': Move as far to the left as possible')
                        elif i['lane_position']=='center_lane':
                            self.vehicle_supervisory_module.vehicle.control_module.set_lateral_offset(self.lane_offset)
                            util.gui_print('Car '+str(self.id)+': Move as far to the left as possible')
                        elif i['lane_position']=='left_lane':
                            self.vehicle_supervisory_module.vehicle.control_module.set_lateral_offset(-self.lane_offset)
                            util.gui_print('Car '+str(self.id)+': Move as far to the right as possible')
                elif i['event_type']==95 and self.lane=='center_lane':
                        if i['lane_position']=='right_lane':
                            util.gui_print('Car '+str(self.id)+': Move as far to the left as possible')
                            self.vehicle_supervisory_module.vehicle.control_module.set_lateral_offset(self.lane_offset)
                        elif i['lane_position']=='center_lane':
                            self.vehicle_supervisory_module.vehicle.control_module.set_lateral_offset(-self.lane_offset)
                            util.gui_print('Car '+str(self.id)+': Move as far to the right as possible')
                        elif i['lane_position']=='left_lane':
                            self.vehicle_supervisory_module.vehicle.control_module.set_lateral_offset(-self.lane_offset)
                            util.gui_print('Car '+str(self.id)+': Move as far to the right as possible')
                return True
        return False

    # Prints the instructions on the HMI to go back after the EV left
    def return_back(self):
        util.gui_print('Car: '+str(self.id)+' Move back to your original position!')
        self.vehicle_supervisory_module.vehicle.control_module.set_lateral_offset(0)

        return True

    # Functions for normal cars to forward an EV message
    def send_EV_message_cars(self):
        if self.EV_lane is not None:
            self.vehicle_supervisory_module.vehicle.v2v_wifi_output=[]
            message_dict=dict()
            message_dict['time']=self.time_ev_leaving
            message_dict['station_type']=5
            message_dict['event_type']=95
            message_dict['lane_position']=self.EV_lane
            self.vehicle_supervisory_module.vehicle.v2v_wifi_output.append(message_dict)

    # Sets controller for all situations except of the "avoiding" since we use only CC controller during "avoiding"
    def set_control(self,velocity):
        if hasattr(self.vehicle_supervisory_module.vehicle,'control_module'):
            # if platoon leader, set CC controller
            if self.platoon_leader:
                if self.lane=='left_lane':
                    self.vehicle_supervisory_module.vehicle.setDesiredVelocity(velocity/3.6)#*0.925)
                else:
                    self.vehicle_supervisory_module.vehicle.setDesiredVelocity(velocity/3.6)
                self.vehicle_supervisory_module.set_CC_control(self.vehicle_supervisory_module.vehicle.desired_velocity)
                self.control_set = 1

                return True
            # if not, set CACC
            else:
                self.vehicle_supervisory_module.set_CACC_control(self.id-1,self.desired_distance)
                self.control_set = 1

                return True

        return False

    # changes a hybrid automaton state and resets flags
    def change_state(self,state):
        self.current_state = state
        self.control_set = 0

        return True

    def get_trace_data(self):
        # return the data one wants to save in the trace file
        trace_data={'hybrid state':self.current_state,'abort':self.abort.state,'velocity':self.vehicle_supervisory_module.get_velocity(),'position':self.vehicle_supervisory_module.get_position()}
        return trace_data
