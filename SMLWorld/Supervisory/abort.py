# Abort automaton
# written by Lars Lindemann
# edited by Sebastian, Sofie and David
import math
import time
import vehiclesupervisorymodule
import Supervisor1
import Supervisor2
import Supervisor3
import util
from Constants import *

class Safety:
    FWD_COLLISION_EMERGENCY_BREAKING    = 16 # check for a collision with an emergency breaking fwd partner or with the RSU
    FWD_PAIR_EMERGENCY_BREAKING         = 17
    BWD_PAIRING_INFORMATION_CONSISTENCY = 18

class abort:
    # written by Lars Lindemann
    def __init__(self,vehicle_supervisory_module):

        self.vehicle_supervisory_module = vehicle_supervisory_module
        self.time = time.time()

        self.state = 1
        self.printed = False #for debugging

        self.safety = []

        self.last_time=None # last time-id for when the message was updated
        self.cz_arrive_late=None # flag indicating if the car is too slow/fast to the CZ: 1 if too slow, 2 if too fast, none if in time.
        self.cz_flag=False # flag indicating if the CZ-approach was correct. True if the car was too slow/fast, false if it was on time or the time to check has not exceeded yet.

        self.safety_distance1=None # distance for when abort should be initiated
        self.safety_distance2=None # distance for which CA is needed
        self.safety_velocity=None #velocity for which it is safe to go to manual
        self.update_safety_variables() #set the safety variables above
        self.safety_distance_target=5 # minimal distance allowed to the target, can be changed from scenario using set_safety12_distance

        self.uncertainty=5 #must be defined as the accepted distance from the CZ-radius
        self.cz_radius=100 # must be defined as the radius of the CZ
        self.time_exceeded=False # false as long as no scenario has sent a warning indicating that the car has been in a state too long
        self.com_error=False # false as long as there are no com.errors, true if there was an error

        self.state_time=None # time when the transition to a certain state was made
        self.max_time=10 # must be defined, maximal time for which it is ok to have no comm.

        self.counter=0 #used to ensure that we don't get stuck in a state due to errors not being true anymore
        self.previuos_state=1
        self.max_distance_to_RSU = 30.0

        self.disabled_safety_check_set = set()
        self.check_time=time.time()
        self.error=[]
        self.safety13 = False
    # written by Lars Lindemann
    # edited by Sebastian Hakansson
    def check_safety(self, ID):
        # delivers true if there is a hazard
        # edited by David
        # even if you want to check just one safety put it in [] otherwise the "if number in ID" won't work
        self.safety=[] #empty the list so old errors are not saved

        for i in range(1,19):
            if i in ID and not (i in self.disabled_safety_check_set):
                method = getattr(self, "check_safety"+str(i))
                if method():
                    self.safety.append(i)

        if len(self.safety)==0:
            return False
        return True
    # written by Lars Lindemann
    #edited by Sofie
    def check_transitions(self):
        if self.state==1: #in scenario
            #print 'Abort state 1 (scenario)'
            if self.check_EoS():
                self.previuos_state=self.state
                self.state=11
            elif self.check_safety([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]): #if any safety applies
                self.previuos_state=self.state
                self.state=2
            elif self.vehicle_supervisory_module.GCDC_module.scenario==1:
                if self.check_safety([16]): # Emergency brake in case we are too close to the RSU or vehicle in front emergency break
                    self.previuos_state=self.state
                    self.state=2   # Goto to emergency braking
                elif self.check_safety([17]): # Emergency brake of the forward pair
                    self.previuos_state=self.state
                    self.state=2   # Go back to init state
                elif self.check_safety([Safety.BWD_PAIRING_INFORMATION_CONSISTENCY]): # Emergency brake of the forward pair
                    self.previuos_state=self.state
                    self.state=2   # Go back to init state
            else:
                return True
        elif self.state==2: #This state doesn't do anything
            #print 'Abort state 2 (there is an error)'
            if self.vehicle_supervisory_module.GCDC_module.scenario==1:
                if self.check_safety([17]): # Emergency brake of the forward pair
                    print str(  self.vehicle_supervisory_module.vehicle.id) + " Reset Scenario from 17"
                    self.vehicle_supervisory_module.GCDC_module.scenario_module.reset_scenario()
                    self.previuos_state=self.state
                    self.state=1   # Go back to init state
                    self.abort_actions()
                elif self.check_safety([Safety.BWD_PAIRING_INFORMATION_CONSISTENCY]): # Emergency brake of the forward pair
                    self.vehicle_supervisory_module.GCDC_module.scenario_module.reset_scenario()
                    self.previuos_state=self.state
                    self.state=1   # Go back to init state
                    self.abort_actions()
                else:
                    self.abort_actions()
                    self.previuos_state=self.state
                    self.state=3
                    self.state_time=time.time()
            else:
                self.abort_actions()
                self.previuos_state=self.state
                self.state=3
                self.state_time=time.time()
        elif self.state==3: #HMI
            #print 'Abort state 3 (Warning will be printed to the HMI)'
            self.abort_actions()
            self.update_safety_variables()
            #Send HMI message
            if self.check_safety([13]):
                self.safety13 = True
            if self.check_safety([1,2,3,6,8,9,10,11]) or (self.check_safety([13]) and (time.time()-self.state_time)>self.max_time):
                self.previuos_state=self.state
                self.state=7
                self.counter=0
            elif self.check_safety([12]) and self.vehicle_supervisory_module.target_distance<=self.safety_distance1:
                self.previuos_state=self.state
                self.state=7
                self.counter=0
            elif (self.check_safety([12]) and self.vehicle_supervisory_module.target_distance>self.safety_distance1) or self.check_safety([15]):
                self.previuos_state=self.state
                self.state=4
                self.counter=0
                self.state_time=time.time()
            elif self.check_safety([7]): # Lars: and also not safety[13]
                self.counter=0
                self.previuos_state=self.state
                self.state=5
            elif self.check_safety([4]): # Lars: and also not safety[13]
                self.previuos_state=self.state
                self.state=6
                self.counter=0
            elif self.check_safety([14]) and not self.check_safety([5,13]):
                self.previuos_state=self.state
                self.state=1
                self.abort_actions()
                self.counter=0
            elif not self.check_safety([13]) and self.safety13:
                self.previuos_state=self.state
                self.state=1
                self.abort_actions()
                self.counter=0
                self.safety13=False
            elif self.vehicle_supervisory_module.GCDC_module.scenario==1:
                self.counter=0
                if self.check_safety([16]): # Emergency brake in ccase we are too close to the RSU
                    self.vehicle_supervisory_module.GCDC_module.scenario_module.reset_scenario()
                    self.previuos_state=self.state
                    self.state=12   # Goto to emergency braking
            else:
                if (time.time()-self.state_time)>self.max_time:
                    self.previuos_state=self.state
                    self.state=1
                    self.abort_actions()
                    #print 'Something is wrong , no safetcheck matched in abort state 3 (HMI)'
        elif self.state==4: #OA
            #print 'Abort state 4 (Obstacle Avoidance in the critical phase)'
            self.abort_actions()
            self.update_safety_variables()
            if self.check_safety([1,2,3,6,8,9,10,11]) or (self.check_safety([13]) and (time.time()-self.state_time)>self.max_time) or (self.check_safety([12]) and self.vehicle_supervisory_module.target_distance<=self.safety_distance1):
                self.previuos_state=self.state
                self.state=3
            elif not self.check_safety([13]): #Lars: && not check_safety[12 && not check_safety[15] in scenario 1]
                self.state_time=time.time()
                if self.check_safety([7]):
                    self.previuos_state=self.state
                    self.state=5
                elif not self.check_safety([12]) and not self.check_safety([15]): #if no errors any more 
                    self.previuos_state=self.state
                    self.state=1
                    self.abort_actions()
            else:
                self.previuos_state=self.state
                self.state=4
        elif self.state==5: #drive on comm
            #print 'Abort state 5 (Sensor failure)'
            self.abort_actions()
            self.update_safety_variables()
            #how do we shut of perception?
            if self.check_safety([1,2,3,6,8,9,10,11]) or (self.check_safety([13]) and (time.time()-self.state_time)>self.max_time) or (self.check_safety([12]) and self.vehicle_supervisory_module.target_distance<=self.safety_distance1):
                self.previuos_state=self.state
                self.state=3
            elif self.counter>100:
                self.previuos_state=self.state
                self.vehicle_supervisory_module.perception_error_sim=False
                self.state=1 #when the perception is ignored
                self.abort_actions()
            elif not self.check_safety([7]):
                self.previuos_state=self.state
                self.state=1
                self.abort_actions()
            else:
                self.previuos_state=self.state
                self.counter+=1
        elif self.state==6: # temporal manual - we can't go manual in the implementation - signal on HMI?
            #print 'Abort state 6 (Temporal Manual, pressing gas pedal)'
            self.abort_actions()
            self.update_safety_variables()
            if self.check_safety([1,2,3,6,8,9,10,11]) or (self.check_safety([13]) and (time.time()-self.state_time)>self.max_time) or (self.check_safety([12]) and self.vehicle_supervisory_module.target_distance<=self.safety_distance1):
                self.previuos_state=self.state
                self.state=3
            elif not self.check_safety([4]):
                self.previuos_state=self.state
                self.state=1
                self.abort_actions()
            else:
                self.previuos_state=self.state
                self.state=6
        elif self.state==7: #abort
            #print 'Abort state 7 (Abort phase initiated)'
            self.abort_actions()
            self.update_safety_variables()
            if self.check_safety([1,2,6,8,11,13,14]):
                self.previuos_state=self.state
                self.state=10
            elif self.check_safety([9]) and self.cz_arrive_late==1:
                self.previuos_state=self.state
                self.state=10
            elif self.check_safety([10]) or (self.check_safety([9]) and self.cz_arrive_late==2) or (self.check_safety([12]) and self.vehicle_supervisory_module.target_distance>self.safety_distance2):
                self.previuos_state=self.state
                self.state=8
            elif (self.check_safety([12]) and self.vehicle_supervisory_module.target_distance<=self.safety_distance2) or self.check_safety([3]):
                self.previuos_state=self.state
                self.state=9
            else:
                return True
        elif self.state==8: # OA
            #print 'Abort state 8 (Obstacle Avoidance in abort phase)'
            self.abort_actions()
            self.update_safety_variables()
            if self.check_safety([11,1,2]): #Lars: do we need to add this in the hybrid automaton
                self.previuos_state=self.state
                self.state=10
            elif self.vehicle_supervisory_module.target_distance<=self.safety_distance2:
                self.previuos_state=self.state
                self.state=9
            elif self.vehicle_supervisory_module.velocity<self.safety_velocity:
                self.previuos_state=self.state
                self.state=10
            else:
                self.previuos_state=self.state
                self.state=8
        elif self.state==9: # CA
            #print 'Abort state 9 (Collision Avoidance)'
            self.abort_actions()
            self.update_safety_variables()
            if self.vehicle_supervisory_module.get_velocity()>=-0.1 and self.vehicle_supervisory_module.get_velocity()<=0.1:
                self.previuos_state=self.state
                self.state=10
            else:
                self.previuos_state=self.state
                self.state=9
        elif self.state==10: # manual
            self.abort_actions()
            if self.check_EoS():
                self.previuos_state=self.state
                self.state=11
        elif self.state==11: #idle
            #print 'Abort state 11 (back to idle)'
            self.abort_actions()
            return True
        elif self.state==12: #idle
            if self.vehicle_supervisory_module.GCDC_module.scenario_module.we_are_leader:
                if self.vehicle_supervisory_module.GCDC_module.scenario_module.can_merge_at_tail():
                    self.vehicle_supervisory_module.GCDC_module.scenario_module.goto(StateScen1.A_WAIT_PAIR)
                    self.vehicle_supervisory_module.release_emergency_brake()
                    self.vehicle_supervisory_module.set_CC_control(self.vehicle_supervisory_module.GCDC_module.scenario_module.scenario_velocity)
                    self.vehicle_supervisory_module.GCDC_module.scenario_module.remove_safety(Safety.FWD_COLLISION_EMERGENCY_BREAKING)
                    self.previuos_state=self.state
                    self.state = 1
                    self.abort_actions()
            return True


    #Acting on states, written by Sofie Andersson
    def abort_actions(self):
        if self.state==1:
            if self.previuos_state!=self.state:
                b=''
                error_list=['emergency', 'manual', 'brake', ' safety 4 (driver using gas pedal),', 'steering', 'system',
                            ' safety 7 (sensor failure),', 'control', 'CZ arrival', 'intersection arrival', 'CAM',
                            ' safety 12 (too close to the object ahead),',' safety 13 (communication failure),',
                            ' safety 14 (spent too long time in a state),', 'force merge', ' safety 16 (too close to the RSU without STOM),',
                            ' safety 17 (vehicle ahead stopped),', ' safety 18 (inconsistent information rearding pairing),']
                for i in [4,7,12,13,14,16,17,18]:
                    if i in self.error:
                        b=b+error_list[i-1]
                a='-------------------------- \n'+'car: '+str(self.vehicle_supervisory_module.vehicle.id)+': The error:'+b+' no longer applies. The scenario will be continued.'
                util.gui_print(a)
        elif self.state==2:
            return True
        elif self.state==3:
            if self.previuos_state!=self.state:
                a='-------------------------- \n'+'car: '+str(self.vehicle_supervisory_module.vehicle.id)+': Abort action 3 - HMI warning print:'
                util.gui_print(a)
                #warning messages
                error_report=['Driver pressed emergency button, manual driving is initiated.',
                            'Driver pressed button for manual control, manual driving is initiated.',
                            'Driver is pressing the brake pedal, Collision Avoidance control is initiated, manual control and abort of the scenario will follow.',
                            'Driver is pressing gas pedal, temporarily manual driving is initiated, automatic control will be resumed when gas pedal is no longer pressed.',
                            'Driver is using the wheel to steer the car, lateral control is aborted, lateral control will be resumed when the wheel is no longer used.',
                            'System failure, manual driving is initiated.',
                            'Sensor failure, the control system will continue driving using the communication module and trusted sensors only.',
                            'Actuator failure, manual driving is initiated.',
                            'The vehicle was not in the specifed area at the given time.',
                            'Most important vehicle was not detected at the intersection within the calculated time, Obstacle avoidance is initiated, be aware of the possibility of the car appearing late.',
                            'Another vehicle is driving on manual control, manual driving will be initiated and the scenario aborted.',
                            'Safety distance to the target vehcile is not fulfilled.',
                            'A message was not updated at the given frequency.',
                            'The allowed time to be in the present state has expired. Press the manual button to abort scenario.',
                            'The forward paired vehicle has determined to force merge despite the fact that it has not received a STOM message. Be aware of a car merging in front of you.',
                            'Did not manage to merge before the RSU!',
                            'The RSU obstacle is too close, emergency brake initiated.',
                            'Pair-vehicle has initiated emergency brake, emergency brake will be initiated.',
                            'Backward pairing inconsistent']
                #some more messages for when the safety divides into different actions
                error_late=': The vehicle had not reached the specified area within the given time contraint. The scenario will be aborted and manual driving is initiated.'
                error_early=': The vehicle had entered too far into the Competition Zone at the given arrival time. Obstacle avoidance is initiated.'
                error_critical_distance=': The distance is considered critical but not dangerous, obstacle avoidance will be intitiated. When the distance is safe the scenario will continue as normal.'
                error_abort_distance=': The distance is considred dangerous, abort state will be initiated and the scenario is aborted.'
                #generate message
                hmi_message=''
                for i in range(1,17):
                    if i in self.error:
                        hmi_message=hmi_message+'\n'+error_report[i-1]
                        if i==9:
                            if self.cz_arrive_late==1:
                                hmi_message=hmi_message+error_late
                            else:
                                hmi_message=hmi_message+error_early
                        elif i==12:
                            if self.vehicle_supervisory_module.target_distance>self.safety_distance1:
                                hmi_message=hmi_message+error_critical_distance
                            else:
                                hmi_message=hmi_message+error_abort_distance
                #print message
                if hmi_message!='':
                    hmi_message=hmi_message+'\n -------------------------- \n'
                    util.gui_print(hmi_message)

        elif self.state==4:
            if self.previuos_state!=self.state:
                a='-------------------------- \n'+'car: '+str(self.vehicle_supervisory_module.vehicle.id)+': Abort action 4 - Obstacle avoidance'
                util.gui_print(a)
                self.previuos_state=self.state
            #a little to close to the car ahead
            #car is about to force merge in front of you
            #set controller to OA
            if self.vehicle_supervisory_module.GCDC_module.scenario is 1:
                ID=self.vehicle_supervisory_module.get_MIO_FWDM()
            elif self.vehicle_supervisory_module.GCDC_module.scenario is 2:
                ID=-1
            else: ID=self.vehicle_supervisory_module.vehicle.id-1
            #set desired distance to the distance used already
            if hasattr(self.vehicle_supervisory_module, 'control_module'):
                if self.vehicle_supervisory_module.type_controller=='OA':
                    distance=self.vehicle_supervisory_module.control_module.OA_desired_distance
                elif self.vehicle_supervisory_module.type_controller=='CACC':
                    distance=self.vehicle_supervisory_module.control_module.CACC_desired_distance
                elif self.vehicle_supervisory_module.type_controller=='VCACC':
                    distance=self.vehicle_supervisory_module.control_module.VCACC_desired_distance
            else: distance=25
            #add some extra distance to the previously used distance to increase gap
            distance=distance+5
            if ID!=None:
                self.vehicle_supervisory_module.set_OA_control(ID, distance)
        elif self.state==5:
            if self.previuos_state!=self.state:
                a='-------------------------- \n'+'car: '+str(self.vehicle_supervisory_module.vehicle.id)+': Abort action 5 - Sensor failure'
                util.gui_print(a)
                self.previuos_state=self.state
            return True
        elif self.state==6: #gas pedal
            if self.previuos_state!=self.state:
                a='-------------------------- \n'+'car: '+str(self.vehicle_supervisory_module.vehicle.id)+': Abort action 6 - Temporal Manual'
                util.gui_print(a)
                self.previuos_state=self.state
            self.vehicle_supervisory_module.set_temp_manual_control()
            return True
        elif self.state==7:
            if self.previuos_state!=self.state:
                a='-------------------------- \n'+'car: '+str(self.vehicle_supervisory_module.vehicle.id)+': Abort action 7 - No way back to the scenario'
                if self.safety13==True:
                    error_communication=' The communication has been down for too long, it is no longer safe to continue driving based on the old information.'
                    a=a+error_communication
                util.gui_print(a)
                self.previuos_state=self.state
            return True
            #do nothing, connects critical with abort
        elif self.state==8:
            if self.previuos_state!=self.state:
                a='-------------------------- \n'+'car: '+str(self.vehicle_supervisory_module.vehicle.id)+': Abort action 8 - Obstacle avoidance'
                util.gui_print(a)
                self.previuos_state=self.state
            if self.vehicle_supervisory_module.GCDC_module.scenario is 1:
                ID=self.vehicle_supervisory_module.get_MIO_FWDM()
            elif self.vehicle_supervisory_module.GCDC_module.scenario is 2:
                ID=-1
            else:
                ID=self.vehicle_supervisory_module.vehicle.id-1
            distance=self.safety_velocity*2 #safety distance for manual
            if ID!=None:
                self.vehicle_supervisory_module.set_OA_control(ID, distance)
        elif self.state==9:
            if self.previuos_state!=self.state:
                a='-------------------------- \n'+'car: '+str(self.vehicle_supervisory_module.vehicle.id)+': Abort action 9 - Collision avoidance'
                util.gui_print(a)
                self.previuos_state=self.state
            self.vehicle_supervisory_module.set_CA_control()
            return True
        elif self.state==10:
            if self.previuos_state!=self.state:
                a ='-------------------------- \n'+ 'car: ' + str(self.vehicle_supervisory_module.vehicle.id) + ': Abort action 10 - manual control'
                util.gui_print(a)
                self.previuos_state=self.state
            self.vehicle_supervisory_module.set_manual_control()
            return True
            #''go to manual''
            # set controller to manual, indicate manual on HMI
        elif self.state==11:
            if self.previuos_state!=self.state:
                a ='-------------------------- \n'+ 'car:' + str(self.vehicle_supervisory_module.vehicle.id) + 'Abort action 11 - go to idle'
                util.gui_print(a)
                self.previuos_state=self.state
            if self.vehicle_supervisory_module.GCDC_module.scenario is 2:
                self.vehicle_supervisory_module.set_CC_control(0)
                self.vehicle_supervisory_module.set_manual_control()
            return True
        elif self.state==12: #emergency brake
            if self.previuos_state!=self.state:
                a='-------------------------- \n'+'car: '+str(self.vehicle_supervisory_module.vehicle.id)+': Abort action 12 - emergency brake'
                util.gui_print(a)
                self.previuos_state=self.state
            self.vehicle_supervisory_module.emergency_brake()
            return True


    def check_safety1(self): #driver press emergency button: low level control disabled
        if self.vehicle_supervisory_module.manual_button_pressed:
            self.error.append(1)
            return True
        else:
            return False

    def check_safety2(self): #driver diables control from HMI: either longitudinal or lateral or both control disabled
        if self.vehicle_supervisory_module.manual_button_pressed:
            self.error.append(2)
            return True
        else:
            return False

    def check_safety3(self): # driver press brake pedal: overrides the controllers and go to manual
        if self.vehicle_supervisory_module.brake_pressed:
            self.error.append(3)
            return True
        else:
            return False

    def check_safety4(self): # driver press gas pedal: temporarily overrides the controllers, back on automatic when unpressed
        if self.vehicle_supervisory_module.gas_pressed:
            self.error.append(4)
            return True
        else:
            return False

    def check_safety5(self):# driver steers car: overrides the lateral controllers temporarily (not implemented in GCDC due to car without lateral control)
        return False

    def check_safety6(self): # system failure- logical failure: go to manual
        if self.vehicle_supervisory_module.system_error_sim:
            self.error.append(6)
            return True
        else:
            return False

    def check_safety7(self):# sensor failure: go to manual
        if self.vehicle_supervisory_module.perception_error or self.vehicle_supervisory_module.perception_error_sim:
            self.error.append(7)
            return True
        else:
            return False

    def check_safety8(self): # control failure: go to manual
        if self.vehicle_supervisory_module.control_error or self.vehicle_supervisory_module.control_error_sim:
            self.error.append(8)
            return True
        else:
            return False

    def check_safety9(self): # vehicle arrives to late or to early to the CZ in scenario 2: abort safely
        #will be checked from checktrans.
        if self.vehicle_supervisory_module.GCDC_module.scenario is 2:
            return self.cz_flag #false if the set_safety9 have not been called yet, or if it was called and the pos was correct, returns true if it was called and the car is not in the allowed zone.
        else:
            return False

    def set_safety9(self): #called from scenario 2 to check if the car is in the allowed zone at time.
        #check if current position is within allowed zone 
        #the distance to intersection doesn't seem to be updated right now.
         if self.vehicle_supervisory_module.distance_to_intersection>self.cz_radius+self.uncertainty: #if too slow
             #print('safety 9 true: too slow')
             self.error.append(9)
             self.cz_flag=True
             self.cz_arrive_late=1
         elif self.vehicle_supervisory_module.distance_to_intersection<self.cz_radius-self.uncertainty: #if too fast
             #print('safety 9 true: too fast')
             self.error.append(9)
             self.cz_flag=True
             self.cz_arrive_late=2
         else: #at right pos
             self.cz_flag=False

    def check_safety10(self):# vehicle 1 does not reach the intersection in calculated time in scenario 2 (will cause crash if not handled)
        #at calculated time, check if perception detects a vehicle ahead, if not: return True - possible to implement?
        if self.vehicle_supervisory_module.GCDC_module.scenario is 2:
            return False # default so far
        else:
            return False

    def check_safety11(self):# CAM indicates that another car is on manual driving: go to manual
        list_of_dicts=self.vehicle_supervisory_module.message_incoming #message: list of dictionaries
        for i in range(0, len(list_of_dicts)-1): #for all received messages
            if list_of_dicts[i]['controller_type']=='manual': #if any car drives with no controller
                self.error.append(11)
                return True
        return False

    def check_safety12(self):# safety distance is not fulfilled - the safety distance should vary depending on if it is virtual or real
        # edited by David, Sofie
        if self.vehicle_supervisory_module.target_distance is not None: # check if the target distance is set
            if self.vehicle_supervisory_module.target_distance<self.safety_distance_target: # if yes, check if it is smaller than 5m
                self.error.append(12)
                return True
            else:
                return False
        else:
            return False

    def set_safety12_distance(self, dist):
        #if self.safety_distance_target: print('12 true')
        self.safety_distance_target=dist

    def check_safety13(self):# communication failure - loss of package/delay
        #check frequency:
        if time.time()-self.check_time>=0.4:
            check_var=True
        else:
            check_var=False
        if check_var:
            self.set_safety13()
            self.check_time=time.time()
        #if self.com_error: print('safety 13 true')
        return self.com_error

    def set_safety13(self):
    #called at certain frequencies from the scenarios, updates the self.com_error
        least_number_of_dicts=self.vehicle_supervisory_module.number_of_cars-1 #number of cars which should send messages
        list_of_dicts=self.vehicle_supervisory_module.message_incoming #message: list of dictionaries
        IDList=[list_of_dicts[0]['vehicle_id'] ] #list of the vehicle IDs
        number_of_diff_car=len(list_of_dicts)
        for i in range(1, len(list_of_dicts)-1): #for each dict check if the ID is in the list: if not-add it, if it is- decrease the number of different cars by one
            if list_of_dicts[i]['vehicle_id'] in IDList:
                number_of_diff_car=number_of_diff_car-1
            else: IDList.append(list_of_dicts[i]['vehicle_id'])
        if number_of_diff_car<least_number_of_dicts: #not all cars have updated
            #a='Car: '+str(self.vehicle_supervisory_module.vehicle.id)+': At least one car did not update the message (13)'
            #util.gui_print(a)
            self.com_error= True
            self.error.append(13)
        else : #all cars are in the list
            if list_of_dicts[0]['time'] is not None and self.last_time is not None:
                if list_of_dicts[0]['time']-self.last_time==0: #no car have updated (same message as checked last)
                    #a='Car: '+str(self.vehicle_supervisory_module.vehicle.id)+ 'The message has not been updated (13)'
                    #util.gui_print(a)
                    self.com_error= True
                    self.error.append(13)
                else: #all cars have updated
                    self.last_time=list_of_dicts[0]['time']
                    self.com_error= False
            else:
                self.last_time = time.time()
        if self.vehicle_supervisory_module.communication_error_sim: #simulated error using button
            self.com_error=True
            self.error.append(13)

    def check_safety14(self): # time in state exceeds allowed time
        #check if time elapsed is greater than time allowed
        return self.time_exceeded # true if the safety was set from the scenario, false otherwise

    def set_safety14(self, start_time, current_time, allowed_time): # sets warning that the time is up
        if current_time-start_time>allowed_time:
            self.time_exceeded=True
            self.error.append(14)
        else:
            self.time_exceeded=False

    def check_safety15(self): #force merge message sent from fwd-partner: go to OA
        #check if fwd partener has sent force merge message: if so return True
        return False

    def check_safety16(self): # avoid collision with the RSU
        # #SorryBadCode
        if self.vehicle_supervisory_module.stationary_vehicle_flag == True:
            return False
        if self.vehicle_supervisory_module.emergency_brake_flag == True:
            return False
        if self.vehicle_supervisory_module.GCDC_module.scenario is 1: #only used within scenario 1
            id_RSU = self.vehicle_supervisory_module.get_RSU().id
            if 'fm' in self.vehicle_supervisory_module.MIO_id and self.vehicle_supervisory_module.MIO_id['fm'] == id_RSU:
                d_RSU = self.vehicle_supervisory_module.vehicle.perception_module.getLongitudinalDistanceToTarget(id_RSU)
                if isinstance(d_RSU,float):
                    if d_RSU<self.max_distance_to_RSU:
                        self.error.append(16)
                        return True

            fwd_m = self.vehicle_supervisory_module.get_fwd_middle()
            if self.vehicle_supervisory_module.is_vehicle_emergency_brake(fwd_m)==True:
                self.error.append(16)
                return True        
        return False


    def check_safety17(self): # vehicle in front or fwd pair emergency brake
        fwd_p = self.vehicle_supervisory_module.vehicle.fwd_pair_partner
        if self.vehicle_supervisory_module.is_vehicle_emergency_brake(fwd_p)==True:
            self.error.append(17)
            return True
        return False

    def check_safety18(self): # Check for consistent bwd pairing information
        if self.vehicle_supervisory_module.GCDC_module.scenario_module.verify_bwd_pairing():
            return False
        else:
            self.error.append(18)
            return True

    def check_EoS(self):
        #check if EoS has been sent: return True
        #should be added to the HMI
        if self.vehicle_supervisory_module.EoS:
            return True
        else:
            return False

    def update_safety_variables(self):
        # set safeties based on the MIO, if there is one
        #return False
        id=self.vehicle_supervisory_module.get_MIO_FWDM()
        if id !=None and self.vehicle_supervisory_module.is_car_ready():
            print 'updating for real'
            target_distance=self.vehicle_supervisory_module.vehicle.perception_module.getLongitudinalDistanceToTarget(id)
            while not velocity_updated:
                message=self.vehicle_supervisory_module.message_incoming
                for i in range(0,len(message)-1):
                    if id==message[i]['vehicle_id']:
                        object_velocity=message[i]['speed']
                        velocity_updated=True
            #safety_velocity is the maximal velocity allowed to go into manual mode. It should correspond to 2s to crash.
            self.safety_velocity=target_distance/2+object_velocity
            print self.safety_velocity
            #safety_distance1 is the maximal distance for which we go to abort. If the distance is greater we will first try OA in the critical state (with chance of return to scenario)
            self.safety_distance1=(self.vehicle_supervisory_module.velocity-object_velocity)*2
            print self.safety_distance1
            #safety_distance2 is the maximal sitance for which CA will be used. If the distance is greater OA will be used to begin with. Should correspond to 1s to crash
            self.safety_distance2=self.vehicle_supervisory_module.velocity-object_velocity
            print self.safety_distance2
        else: #there is no MIO, set the safeties such that you're always allowed to go to manual and no distance to a vehicle ahead causes abort.
            print 'could not update - faulty values set'
            self.safety_distance2=3
            self.safety_distance1=4
            self.safety_velocity=40
