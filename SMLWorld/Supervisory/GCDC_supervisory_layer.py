# This file is the intermediate level responsible for changing the scenario and tunneling the
# scenario information to the scenarios
# Lars Lindemann
'''
      ___           ___                         ___     
     /\__\         /\__\         _____         /\__\    
    /:/ _/_       /:/  /        /::\  \       /:/  /    
   /:/ /\  \     /:/  /        /:/\:\  \     /:/  /     
  /:/ /::\  \   /:/  /  ___   /:/  \:\__\   /:/  /  ___ 
 /:/__\/\:\__\ /:/__/  /\__\ /:/__/ \:|__| /:/__/  /\__\ 
 \:\  \ /:/  / \:\  \ /:/  / \:\  \ /:/  / \:\  \ /:/  /
  \:\  /:/  /   \:\  /:/  /   \:\  /:/  /   \:\  /:/  / 
   \:\/:/  /     \:\/:/  /     \:\/:/  /     \:\/:/  /  
    \::/  /       \::/  /       \::/  /       \::/  /   
     \/__/         \/__/         \/__/         \/__/    
'''

import math
import vehiclesupervisorymodule
import Supervisor1
import Supervisor2
import Supervisor3
import util

class GCDC_supervisory_layer:
    # written by Lars Lindemann
    def __init__(self,vehicle_supervisory_module,scenario):

        self.scenario = scenario
        self.vehicle_supervisory_module = vehicle_supervisory_module

        #Initialize the scenario class
        if scenario==1:
            self.scenario_module=Supervisor1.SupervisorScenario1(self.vehicle_supervisory_module)
        elif self.scenario==2:
            self.scenario_module=Supervisor2.SupervisorScenario2(self.vehicle_supervisory_module)
        elif self.scenario==3:
            self.scenario_module=Supervisor3.SupervisorScenario3(self.vehicle_supervisory_module)

    # written by Lars Lindemann
    def set_scenario(self,scenario):
        self.scenario = scenario
        if scenario==1:
            self.scenario_module=Supervisor1.SupervisorScenario1(self.vehicle_supervisory_module)
        elif self.scenario==2:
            self.scenario_module=Supervisor2.SupervisorScenario2(self.vehicle_supervisory_module)
        elif self.scenario==3:
            self.scenario_module=Supervisor3.SupervisorScenario3(self.vehicle_supervisory_module)

    # written by Lars Lindemann
    def step(self):
        self.scenario_module.execute_scenario()

        util.add_to_trace(self.vehicle_supervisory_module.get_vehicle_id(),self.scenario_module.get_trace_data())